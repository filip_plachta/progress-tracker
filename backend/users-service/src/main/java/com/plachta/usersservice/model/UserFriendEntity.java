package com.plachta.usersservice.model;

import javax.persistence.*;

@Entity
@Table(name = "userFriend", schema = "public")
public class UserFriendEntity {
    private UserFriendId userFriend;

    @EmbeddedId
    public UserFriendId getUserFriend() {
        return userFriend;
    }

    public void setUserFriend(UserFriendId userFriend) {
        this.userFriend = userFriend;
    }
}
