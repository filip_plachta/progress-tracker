package com.plachta.usersservice.dto;

import com.plachta.usersservice.model.UserEntity;

import java.util.Objects;

public class UserResponse {
    private Integer id;
    private String email;
    private String login;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static UserResponse fromEntity(UserEntity entity) {
        UserResponse dto = new UserResponse();
        dto.setEmail(entity.getEmail());
        dto.setId(entity.getId());
        dto.setLogin(entity.getLogin());
        return dto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(login, that.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, login);
    }
}