package com.plachta.usersservice.dao;

import com.plachta.usersservice.model.UserEntity;
import com.plachta.usersservice.model.UserFriendEntity;
import com.plachta.usersservice.model.UserFriendId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface UserFriendRepository extends CrudRepository<UserFriendEntity, UserFriendId> {

    Optional<UserFriendEntity> findByUserFriend_UserAndUserFriend_Friend(UserEntity user, UserEntity friend);
    Boolean existsByUserFriend_UserAndUserFriend_Friend(UserEntity user, UserEntity friend);
    Boolean existsByUserFriend_User_IdAndUserFriend_Friend_Id(Integer userId, Integer friendId);
    void deleteByUserFriend_UserAndUserFriend_Friend(UserEntity user, UserEntity friend);
    Set<UserFriendEntity> findByUserFriend_Friend(UserEntity friend);

    @Query(value = "select f1.user_id, f1.friend_id" +
            " from public.user_friend f1 inner join public.user_friend " +
            "f2 on f1.user_id = f2.friend_id and f1.friend_id = f2.user_id " +
            "where f1.user_id = :userId", nativeQuery = true)
    Set<UserFriendEntity> findFriends(@Param("userId") Integer userId);

}
