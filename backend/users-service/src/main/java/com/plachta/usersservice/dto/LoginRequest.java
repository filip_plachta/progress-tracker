package com.plachta.usersservice.dto;

import javax.validation.constraints.NotNull;

public class LoginRequest {

    @NotNull
    String login;

    @NotNull
    String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
