package com.plachta.usersservice.controller;

import com.plachta.usersservice.dto.UserRequest;
import com.plachta.usersservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/registration")
    public ResponseEntity<?> addUser(@RequestBody UserRequest userRequest) {
        userService.addUser(userRequest);
        return ResponseEntity.ok("User added successfully");
    }

    @GetMapping("/me")
    public ResponseEntity<?> getLoggedUser(Authentication auth) throws Exception {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        return ResponseEntity.ok(userService.getUser(userDetails.getUsername()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(Authentication auth, @PathVariable Integer id) throws Exception {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        return ResponseEntity.ok(userService.getUser(userDetails.getUsername(), id));
    }
}
