package com.plachta.usersservice.controller;

import com.plachta.usersservice.dto.FriendRequest;
import com.plachta.usersservice.dto.UserResponse;
import com.plachta.usersservice.service.UserFriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/friends")
public class UserFriendController {

    @Autowired
    private UserFriendService userFriendService;

    @GetMapping
    public ResponseEntity<?> getUserFriends(Authentication auth) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        return ResponseEntity.ok(userFriendService.getFriends(userDetails.getUsername()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFriendship(@PathVariable Integer id, Authentication auth) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        userFriendService.deleteFriendship(id, userDetails.getUsername());
        return ResponseEntity.ok("Friendship deleted succesfully");
    }

    @PostMapping
    public ResponseEntity<?> addFriendRequest(@RequestBody FriendRequest request, Authentication auth) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        userFriendService.addFriendRequest(request, userDetails.getUsername());
        return ResponseEntity.ok("Friend request has been sent!");
    }

    @GetMapping("/requests")
    public ResponseEntity<?> getFriendRequests(Authentication auth) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        return ResponseEntity.ok(userFriendService.getUserFriendRequests(userDetails.getUsername()));
    }

    @PutMapping("/accept")
    public ResponseEntity<?> acceptFriend(Authentication auth, @RequestBody FriendRequest request) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        userFriendService.acceptRequest(userDetails.getUsername(), request);
        return ResponseEntity.ok("Friend request has been accepted!");
    }

    @PutMapping("/decline")
    public ResponseEntity<?> declineFriend(Authentication auth, @RequestBody FriendRequest request) {
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        userFriendService.declineRequest(userDetails.getUsername(), request);
        return ResponseEntity.ok("Friend request has been declined!");
    }
}
