package com.plachta.usersservice.dto;

import com.plachta.usersservice.model.UserEntity;

public class UserRequest {
    private String email;
    private String login;
    private String password;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static UserRequest fromEntity(UserEntity entity) {
        UserRequest dto = new UserRequest();
        dto.setEmail(entity.getEmail());
        dto.setLogin(entity.getEmail());
        dto.setPassword(entity.getEmail());
        return dto;
    }
}
