package com.plachta.usersservice.service;

import com.plachta.usersservice.dao.UserRepository;
import com.plachta.usersservice.dto.UserRequest;
import com.plachta.usersservice.dto.UserResponse;
import com.plachta.usersservice.exception.BadRequestException;
import com.plachta.usersservice.exception.ResourceNotFoundException;
import com.plachta.usersservice.exception.UnauthorizedRequest;
import com.plachta.usersservice.model.UserEntity;
import com.sun.jersey.api.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserFriendService userFriendService;

    public List<UserResponse> getUsers() {
        return userRepository.findAll()
                .stream()
                .map(e -> UserResponse.fromEntity(e))
                .collect(Collectors.toList());
    }

    public UserResponse getUser(String login) {
        UserEntity entity = userRepository
                .findByLogin(login).get();

        UserResponse response = UserResponse.fromEntity(entity);
        return response;

    }

    public UserResponse getUser(String loggedUser, Integer id) {
        UserEntity entity = userRepository
                .findByLogin(loggedUser).get();
        UserEntity friend = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User was not found"));

        var friends = userFriendService.getFriends(loggedUser);
        UserResponse response = UserResponse.fromEntity(friend);

        if (!friends.contains(response)) {
            throw new UnauthorizedRequest("This user is private");
        }

        return response;

    }

    public void addUser(UserRequest request) {
        if (userRepository.existsByEmail(request.getEmail()) || userRepository.existsByLogin(request.getLogin())) {
            throw new BadRequestException("User with this login or email already exists");
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(request.getEmail());
        userEntity.setLogin(request.getLogin());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        userEntity.setPassword(passwordEncoder.encode(request.getPassword()));
        userRepository.save(userEntity);
    }
}
