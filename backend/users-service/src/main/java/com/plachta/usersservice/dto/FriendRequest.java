package com.plachta.usersservice.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class FriendRequest {
    private String username;

    @NotNull
    @NotBlank
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
