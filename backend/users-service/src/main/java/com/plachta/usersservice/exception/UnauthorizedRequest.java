package com.plachta.usersservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedRequest extends RuntimeException {
    public UnauthorizedRequest(String message) {
        super(message);
    }
}
