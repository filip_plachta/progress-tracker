package com.plachta.usersservice.service;

import com.plachta.usersservice.dao.UserFriendRepository;
import com.plachta.usersservice.dao.UserRepository;
import com.plachta.usersservice.dto.FriendRequest;
import com.plachta.usersservice.dto.UserResponse;
import com.plachta.usersservice.exception.ResourceNotFoundException;
import com.plachta.usersservice.model.UserEntity;
import com.plachta.usersservice.model.UserFriendEntity;
import com.plachta.usersservice.model.UserFriendId;
import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserFriendService {

    @Autowired
    private UserFriendRepository userFriendRepository;

    @Autowired
    private UserRepository userRepository;

    public void addFriendRequest(FriendRequest request, String loggedUser) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        UserEntity friend = userRepository.findByLogin(request.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + request.getUsername()));

        UserFriendId id = new UserFriendId();
        id.setUser(user);
        id.setFriend(friend);
        UserFriendEntity entity = new UserFriendEntity();
        entity.setUserFriend(id);
        userFriendRepository.save(entity);
    }

    public void acceptRequest(String loggedUser, FriendRequest request) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        UserEntity friend = userRepository.findByLogin(request.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + request.getUsername()));

        if (userFriendRepository.existsByUserFriend_UserAndUserFriend_Friend(friend, user)) {
            UserFriendId id = new UserFriendId();
            id.setUser(user);
            id.setFriend(friend);
            UserFriendEntity entity = new UserFriendEntity();
            entity.setUserFriend(id);
            userFriendRepository.save(entity);
        }
    }

    @Transactional
    public void declineRequest(String loggedUser, FriendRequest request) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        UserEntity friend = userRepository.findByLogin(request.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + request.getUsername()));

        userFriendRepository.deleteByUserFriend_UserAndUserFriend_Friend(friend, user);
    }

    public void sendRequest(String loggedUser, FriendRequest request) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        UserEntity friend = userRepository.findByLogin(request.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + request.getUsername()));
        UserFriendId id = new UserFriendId();
        id.setUser(user);
        id.setFriend(friend);
        UserFriendEntity entity = new UserFriendEntity();
        entity.setUserFriend(id);
        userFriendRepository.save(entity);
    }

    public Set<UserResponse> getFriends(String loggedUser) {
        UserEntity entity = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        var friends = userFriendRepository.findFriends(entity.getId());
        Set<UserResponse> response = friends
                .stream()
                .map((UserFriendEntity userFriend) -> UserResponse.fromEntity(userFriend.getUserFriend().getFriend()))
                .collect(Collectors.toSet());
        return response;
    }

    @Transactional
    public void deleteFriendship(Integer friendId, String loggedUser) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));
        UserEntity friend = userRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("Wrong user id: " + friendId));

        userFriendRepository.deleteByUserFriend_UserAndUserFriend_Friend(user, friend);
        userFriendRepository.deleteByUserFriend_UserAndUserFriend_Friend(friend, user);
    }

    public Set<UserResponse> getUserFriendRequests(String loggedUser) {
        UserEntity user = userRepository.findByLogin(loggedUser)
                .orElseThrow(() -> new ResourceNotFoundException("User does not exist: " + loggedUser));

        var list = userFriendRepository.findByUserFriend_Friend(user);
        var userFriends = userFriendRepository.findFriends(user.getId());
        Set<UserResponse> response = new HashSet<>();

        for (UserFriendEntity userFriend: list) {
            if (!userFriendRepository
                    .existsByUserFriend_UserAndUserFriend_Friend(userFriend.getUserFriend().getFriend(),
                            userFriend.getUserFriend().getUser())) {
                response.add(UserResponse.fromEntity(userFriend.getUserFriend().getUser()));
            }
        }

        return response;

    }
}
