INSERT INTO public.user
(email, login, "password", id, joined_on)
VALUES('email', 'user', '$2a$10$y70awehDgksJIeIZePGjZ.QgT6UKWkoJ2GRJUz/7/Z6l/hTQaWD.K', 1, '2020-12-28 16:28:20.000000');
INSERT INTO public.user
(email, login, "password", id, joined_on)
VALUES('email2', 'user2', '$2a$10$y70awehDgksJIeIZePGjZ.QgT6UKWkoJ2GRJUz/7/Z6l/hTQaWD.K', 2, '2020-12-28 16:28:20.000000');
INSERT INTO public.user
(email, login, "password", id, joined_on)
VALUES('email3', 'user3', '$2a$10$y70awehDgksJIeIZePGjZ.QgT6UKWkoJ2GRJUz/7/Z6l/hTQaWD.K', 3, '2020-12-28 16:28:20.000000');

INSERT INTO public.user_friend
(user_id, friend_id)
VALUES(1,2);

INSERT INTO public.user_friend
(user_id, friend_id)
VALUES(2,1);

INSERT INTO public.user_friend
(user_id, friend_id)
VALUES(1,3);

INSERT INTO public.user_friend
(user_id, friend_id)
VALUES(3,1);

INSERT INTO public.user_friend
(user_id, friend_id)
VALUES(3,2);
