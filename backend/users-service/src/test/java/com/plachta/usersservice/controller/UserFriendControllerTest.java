package com.plachta.usersservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.plachta.usersservice.dto.FriendRequest;
import com.plachta.usersservice.dto.UserRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.*;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class UserFriendControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @WithMockUser("user")
    public void getUserFriendsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/friends")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].email").value("email2"))
                .andExpect(jsonPath("$[0].login").value("user2"))
                .andExpect(jsonPath("$[1].id").value(3))
                .andExpect(jsonPath("$[1].email").value("email3"))
                .andExpect(jsonPath("$[1].login").value("user3"));
    }

    @Test
    @WithMockUser("user")
    public void deleteFriendshipTest() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.delete("/friends/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andReturn();
        String content = result.getResponse().getContentAsString();
        if (!content.equals("Friendship deleted succesfully")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    @WithMockUser("user")
    public void addFriendRequestTestAndDisplay() throws Exception {
        FriendRequest request = new FriendRequest();
        request.setUsername("user2");
        var result = mvc.perform(MockMvcRequestBuilders.post("/friends")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        if (!content.equals("Friend request has been sent!")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    @WithMockUser("user2")
    public void getFriendRequestsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/friends/requests")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].login").value("user3"));
    }

    @Test
    @WithMockUser("user2")
    public void acceptFriendRequestTest() throws Exception {
        FriendRequest request = new FriendRequest();
        request.setUsername("user3");
        var result = mvc.perform(MockMvcRequestBuilders.put("/friends/accept")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        if (!content.equals("Friend request has been accepted!")) {
            throw new Exception("Wrong message");
        }

    }

    @Test
    @WithMockUser("user2")
    public void declineFriendRequestTest() throws Exception {
        FriendRequest request = new FriendRequest();
        request.setUsername("user3");
        var result = mvc.perform(MockMvcRequestBuilders.put("/friends/decline")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        if (!content.equals("Friend request has been declined!")) {
            throw new Exception("Wrong message");
        }

    }

}