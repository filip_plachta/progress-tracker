package com.plachta.usersservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.plachta.usersservice.dto.UserRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.*;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void addUserTest() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setLogin("User1232");
        userRequest.setPassword("password");
        userRequest.setEmail("email123");
        var result = mvc.perform(MockMvcRequestBuilders.post("/user/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequest)))
                .andExpect(status().is(200))
                .andReturn();
        String content = result.getResponse().getContentAsString();
        if (!content.equals("User added successfully")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    @WithMockUser("user")
    public void getLoggedUserTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/user/me")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.email").value("email"))
                .andExpect(jsonPath("$.login").value("user"));
    }

    @Test
    @WithMockUser("user")
    public void getUserTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/user/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.email").value("email2"))
                .andExpect(jsonPath("$.login").value("user2"));
    }
}