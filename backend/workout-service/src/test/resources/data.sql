INSERT INTO public.workout_routine
(id, created, name, "owner", is_private)
VALUES(1, '2020-12-12 00:26:04.907', 'Chest', 1, false);

INSERT INTO public.workout_routine
(id, created, name, "owner", is_private)
VALUES(2, '2020-12-12 00:26:04.907', 'Back', 2, false);

INSERT INTO public.user_workout_routine
(user_id, workout_routine_id)
VALUES (1,1);

INSERT INTO public.workout_exercise
(id, exercise_name, max_reps, min_reps, "sets", workout_routine_id)
VALUES(1, 'Bench Press', 6, 3, 4, 1);

INSERT INTO public.workout_session
(id, "date", user_id, routine_id)
VALUES(1, '2020-12-12 18:54:42.000', 1, 1);

INSERT INTO public.workout_session_exercise
(id, notes, reps, "set", workout_exercise_id, workout_session_id, weight)
VALUES(1, NULL, 6, 1, 1, 1, 50.00);

INSERT INTO public.workout_session_exercise
(id, notes, reps, "set", workout_exercise_id, workout_session_id, weight)
VALUES(2, NULL, 5, 2, 1, 1, 50.00);

INSERT INTO public.workout_session_exercise
(id, notes, reps, "set", workout_exercise_id, workout_session_id, weight)
VALUES(3, NULL, 2, 3, 1, 1, 50.00);




