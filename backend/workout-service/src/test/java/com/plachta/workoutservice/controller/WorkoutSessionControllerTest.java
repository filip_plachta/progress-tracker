package com.plachta.workoutservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.plachta.workoutservice.dto.*;
import com.plachta.workoutservice.security.JwtRequestFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class WorkoutSessionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RestTemplate restTemplate;

    @Before
    public void setUpRestRemplate() throws Exception {
        UserResponse response = new UserResponse();
        response.setEmail("email");
        response.setLogin("user");
        response.setId(1);
        Mockito
                .when(restTemplate.exchange(Mockito.anyString(), Mockito.<HttpMethod>any(), Mockito.<HttpEntity>any(), Mockito.<Class<Object>>any()))
                .thenReturn(ResponseEntity.ok(response));
    }

    @Test
    public void getMyWorkoutSessionsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/session")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].routine").value("Chest"))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].date").value("2020-12-12T17:54:42.000+00:00"));

    }

    @Test
    public void getUserWorkoutSessionsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/session/user/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].routine").value("Chest"))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].date").value("2020-12-12T17:54:42.000+00:00"));

    }

    @Test
    public void getWorkoutSessionTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/session/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.routine").value("Chest"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.date").value("2020-12-12T17:54:42.000+00:00"))
                .andExpect(jsonPath("$.exercises[0].set").value(1))
                .andExpect(jsonPath("$.exercises[0].exerciseName").value("Bench Press"))
                .andExpect(jsonPath("$.exercises[0].weight").value(50.00))
                .andExpect(jsonPath("$.exercises[0].reps").value(6))

                .andExpect(jsonPath("$.exercises[1].set").value(2))
                .andExpect(jsonPath("$.exercises[1].exerciseName").value("Bench Press"))
                .andExpect(jsonPath("$.exercises[1].weight").value(50.00))
                .andExpect(jsonPath("$.exercises[1].reps").value(5))

                .andExpect(jsonPath("$.exercises[2].set").value(3))
                .andExpect(jsonPath("$.exercises[2].exerciseName").value("Bench Press"))
                .andExpect(jsonPath("$.exercises[2].weight").value(50.00))
                .andExpect(jsonPath("$.exercises[2].reps").value(2));


    }

    @Test
    public void initWorkoutSession() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/session/init/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))

                .andExpect(jsonPath("$.exercises[0].exercise.sets").value(4))
                .andExpect(jsonPath("$.exercises[0].exercise.exerciseName").value("Bench Press"))
                .andExpect(jsonPath("$.exercises[0].exercise.minReps").value(3))
                .andExpect(jsonPath("$.exercises[0].exercise.maxReps").value(6))

                .andExpect(jsonPath("$.exercises[0].goals[0].set").value(1))
                .andExpect(jsonPath("$.exercises[0].goals[0].weight").value(52.50))
                .andExpect(jsonPath("$.exercises[0].goals[0].reps").value(3))

                .andExpect(jsonPath("$.exercises[0].goals[1].set").value(2))
                .andExpect(jsonPath("$.exercises[0].goals[1].weight").value(50.00))
                .andExpect(jsonPath("$.exercises[0].goals[1].reps").value(6))

                .andExpect(jsonPath("$.exercises[0].goals[2].set").value(3))
                .andExpect(jsonPath("$.exercises[0].goals[2].weight").value(47.50))
                .andExpect(jsonPath("$.exercises[0].goals[2].reps").value(6));

    }

    @Test
    public void createWorkoutSessionTest() throws Exception {
        WorkoutTemplateDTO dto = new WorkoutTemplateDTO();
        List<ExerciseTemplateDTO> exercises = new ArrayList<>();
        ExerciseTemplateDTO exercise = new ExerciseTemplateDTO();
        List<ExerciseGoalDTO> goals = new ArrayList<>();
        ExerciseGoalDTO goal = new ExerciseGoalDTO();
        goal.setSet(1);
        goal.setPerformedReps(6);
        goal.setPerformedWeight(BigDecimal.valueOf(50.50));
        goals.add(goal);
        exercise.setGoals(goals);
        WorkoutExerciseRequestResponse x = new WorkoutExerciseRequestResponse();
        x.setExerciseName("Bench Press");
        x.setId(1);
        exercise.setExercise(x);
        exercises.add(exercise);
        dto.setExercises(exercises);
        var result = mvc.perform(MockMvcRequestBuilders.post("/session/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().is(200))
                .andReturn();
        var response = result.getResponse().getContentAsString();
        if (!response.equals("Session has been ended!")) {
            throw new Exception("Wrong message");
        }

    }
}
