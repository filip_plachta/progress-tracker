package com.plachta.workoutservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.plachta.workoutservice.dto.UserResponse;
import com.plachta.workoutservice.dto.WorkoutExerciseRequestResponse;
import com.plachta.workoutservice.dto.WorkoutRoutineRequestResponse;
import com.plachta.workoutservice.security.JwtRequestFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

import java.util.Collections;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class WorkoutRoutineControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RestTemplate restTemplate;

    @Before
    public void setUpRestRemplate() throws Exception {
        UserResponse response = new UserResponse();
        response.setEmail("email");
        response.setLogin("user");
        response.setId(1);
        Mockito
                .when(restTemplate.exchange(Mockito.anyString(), Mockito.<HttpMethod>any(), Mockito.<HttpEntity> any(), Mockito.<Class<Object>> any()))
                .thenReturn(ResponseEntity.ok(response));
    }

    @Test
    public void getMyWorkoutRoutinesTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/routine")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("Chest"))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].created").value("2020-12-11T23:26:04.907+00:00"));
    }

    @Test
    public void getUserWorkoutRoutines() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/routine/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("Chest"))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].created").value("2020-12-11T23:26:04.907+00:00"));
    }

    @Test
    public void linkWithRoutineTest() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.post("/routine/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andReturn();
        var response = result.getResponse().getContentAsString();
        if (!response.equals("Routine has been added!")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    public void unlinkWithRoutineTest() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.delete("/routine/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andReturn();
        var response = result.getResponse().getContentAsString();
        if (!response.equals("Routine has been deleted!")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    public void addUserWorkoutRoutine() throws Exception {
        WorkoutRoutineRequestResponse request = new WorkoutRoutineRequestResponse();
        request.setIsPrivate(false);
        request.setName("Routine");
        WorkoutExerciseRequestResponse ex = new WorkoutExerciseRequestResponse();
        ex.setSets(4);
        ex.setMaxReps(10);
        ex.setMinReps(10);
        ex.setExerciseName("Exercise Name");
        request.setExercises(Collections.singleton(ex));
        var result = mvc.perform(MockMvcRequestBuilders.post("/routine")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().is(200))
                .andReturn();
        var response = result.getResponse().getContentAsString();
        if (!response.equals("New workout routine has been added")) {
            throw new Exception("Wrong message");
        }
    }

    @Test
    public void getRoutineDetailsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/routine/details/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name").value("Chest"))
                .andExpect(jsonPath("$.isPrivate").value(false))
                .andExpect(jsonPath("$.exercises[0].exerciseName").value("Bench Press"))
                .andExpect(jsonPath("$.exercises[0].minReps").value(3))
                .andExpect(jsonPath("$.exercises[0].maxReps").value(6))
                .andExpect(jsonPath("$.exercises[0].sets").value(4));

    }
}
