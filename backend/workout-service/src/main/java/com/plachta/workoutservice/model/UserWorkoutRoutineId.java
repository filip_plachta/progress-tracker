package com.plachta.workoutservice.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserWorkoutRoutineId implements Serializable {

    private Integer workoutRoutineId;
    private Integer userId;

    @Column(name="workoutRoutineId")
    public Integer getWorkoutRoutineId() {
        return workoutRoutineId;
    }

    public void setWorkoutRoutineId(Integer workoutRoutineIdd) {
        this.workoutRoutineId = workoutRoutineIdd;
    }

    @Column(name = "userId")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
