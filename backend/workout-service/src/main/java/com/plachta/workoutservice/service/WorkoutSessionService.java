package com.plachta.workoutservice.service;

import com.plachta.workoutservice.dao.WorkoutExerciseRepository;
import com.plachta.workoutservice.dao.WorkoutRoutineRepository;
import com.plachta.workoutservice.dao.WorkoutSessionRepository;
import com.plachta.workoutservice.dto.*;
import com.plachta.workoutservice.exception.ResourceNotFoundException;
import com.plachta.workoutservice.model.WorkoutExerciseEntity;
import com.plachta.workoutservice.model.WorkoutRoutineEntity;
import com.plachta.workoutservice.model.WorkoutSessionEntity;
import com.plachta.workoutservice.model.WorkoutSessionExerciseEntity;
import com.sun.jersey.api.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WorkoutSessionService {

    @Autowired
    private WorkoutSessionRepository workoutSessionRepository;

    @Autowired
    private WorkoutRoutineRepository workoutRoutineRepository;

    @Autowired
    private WorkoutExerciseRepository workoutExerciseRepository;

    public Set<WorkoutSessionResponse> getUserWorkoutSessions(Integer userId) {
        var workoutSessions = workoutSessionRepository.findAllByUserIdOrderByDateDesc(userId);
        Set<WorkoutSessionResponse> response = workoutSessions
                .stream()
                .map(WorkoutSessionResponse::fromEntity)
                .collect(Collectors.toSet());
        return response;
    }

//    @Transactional
//    public List<SessionExerciseRequestResponse> initWorkoutSession(Integer routineId, Integer userId) {
//        Optional<WorkoutSessionEntity> latestSession = workoutSessionRepository
//                .findTopByRoutine_IdAndUserIdOrderByDateDesc(routineId, userId);
//        List<SessionExerciseRequestResponse> toDoExercises = new ArrayList<>();
//        if (latestSession.isPresent()) {
//            var lastExercises = latestSession.get().getExercises();
//            if (lastExercises != null) {
//                for (WorkoutSessionExerciseEntity performedExercise : lastExercises) {
//                    WorkoutExerciseEntity exerciseEntity = performedExercise.getWorkoutExercise();
//                    SessionExerciseRequestResponse element = new SessionExerciseRequestResponse();
//                    element.setSet(performedExercise.getSet());
//                    element.setExercise(WorkoutExerciseRequestResponse.fromEntity(performedExercise.getWorkoutExercise()));
//                    if (performedExercise.getReps() >= exerciseEntity.getMaxReps()) {
//                        // podnies ciezar x min Reps
//                        element.setReps(exerciseEntity.getMinReps());
//                        BigDecimal newWeight = performedExercise.getWeight().add(BigDecimal.valueOf(2.5));
//                        element.setWeight(newWeight);
//                    } else if (performedExercise.getReps() < exerciseEntity.getMinReps()) {
//                        // zmniejsz ciezar x max Reps
//                        element.setReps(exerciseEntity.getMaxReps());
//                        BigDecimal newWeight = performedExercise.getWeight().subtract(BigDecimal.valueOf(2.5));
//                        if (newWeight.compareTo(BigDecimal.valueOf(0)) < 1) {
//                            newWeight = BigDecimal.valueOf(0);
//                        }
//                        element.setWeight(newWeight);
//                    } else {
//                        // zostaw ciezar x +1 powt
//                        element.setReps(performedExercise.getReps() + 1);
//                        element.setWeight(performedExercise.getWeight());
//                    }
//                    toDoExercises.add(element);
//                }
//            }
//        }
//        return toDoExercises;
//    }

    public WorkoutTemplateDTO createWorkoutTemplate(Integer routineId, Integer userId) {
        Optional<WorkoutSessionEntity> latestSession = workoutSessionRepository
                .findTopByRoutine_IdAndUserIdOrderByDateDesc(routineId, userId);
        WorkoutTemplateDTO templateDTO = new WorkoutTemplateDTO();
        if (latestSession.isPresent()) {
            WorkoutSessionEntity session = latestSession.get();
            var exercises = session.getRoutine().getExercises();

            List<ExerciseTemplateDTO> exerciseTemplateDTOS = new ArrayList<>();
            for (var exercise : exercises) {
                var latestVolume = session.getExercises()
                        .stream()
                        .filter((WorkoutSessionExerciseEntity e) -> e.getWorkoutExercise().getId().equals(exercise.getId()))
                        .collect(Collectors.toSet());
                ExerciseTemplateDTO exerciseTemplate = new ExerciseTemplateDTO();
                exerciseTemplate.setExercise(WorkoutExerciseRequestResponse.fromEntity(exercise));
                List<ExerciseGoalDTO> goals = new ArrayList<>();
                for (int i = 1; i <= exercise.getSets(); i++) {
                    ExerciseGoalDTO goal = new ExerciseGoalDTO();
                    goal.setSet(i);
                    goals.add(goal);
                }

                for (var latestExercise : latestVolume) {
                    var foundGoals = goals.stream().filter(el -> el.getSet().equals(latestExercise.getSet())).collect(Collectors.toList());

                    if (foundGoals != null) {
                        var goal = foundGoals.get(0);
                        if (latestExercise.getReps() >= exercise.getMaxReps()) {
                            goal.setReps(exercise.getMinReps());
                            goal.setSet(latestExercise.getSet());
                            BigDecimal newWeight = latestExercise.getWeight().add(BigDecimal.valueOf(2.5));
                            goal.setWeight(newWeight);
                        } else if (latestExercise.getReps() < exercise.getMinReps()) {
                            goal.setReps(exercise.getMaxReps());
                            goal.setSet(latestExercise.getSet());
                            BigDecimal newWeight = latestExercise.getWeight().subtract(BigDecimal.valueOf(2.5));
                            if (newWeight.compareTo(BigDecimal.valueOf(0)) < 1) {
                                newWeight = BigDecimal.valueOf(0);
                            }
                            goal.setWeight(newWeight);
                        } else {
                            goal.setWeight(latestExercise.getWeight());
                            goal.setSet(latestExercise.getSet());
                            goal.setReps(latestExercise.getReps() + 1);
                        }
                    }
                }
                exerciseTemplate.setGoals(goals);
                exerciseTemplateDTOS.add(exerciseTemplate);
            }
            templateDTO.setExercises(exerciseTemplateDTOS);
        } else {
            WorkoutRoutineEntity routine = workoutRoutineRepository
                    .findById(routineId)
                    .orElseThrow(() -> new ResourceNotFoundException("Wrong workout routine!"));
            var exercises = routine.getExercises();
            List<ExerciseTemplateDTO> exerciseTemplateDTOS = new ArrayList<>();
            for (var exercise : exercises) {
                ExerciseTemplateDTO exerciseTemplate = new ExerciseTemplateDTO();
                exerciseTemplate.setExercise(WorkoutExerciseRequestResponse.fromEntity(exercise));
                List<ExerciseGoalDTO> goals = new ArrayList<>();
                for (int i = 1; i <= exercise.getSets(); i++) {
                    ExerciseGoalDTO goal = new ExerciseGoalDTO();
                    goal.setSet(i);
                    goals.add(goal);
                }
                exerciseTemplate.setGoals(goals);
                exerciseTemplateDTOS.add(exerciseTemplate);
            }
            templateDTO.setExercises(exerciseTemplateDTOS);
        }
        return templateDTO;
    }

    public void saveWorkoutSession(Integer userId, Integer routineId, WorkoutTemplateDTO request) {
        WorkoutSessionEntity session = new WorkoutSessionEntity();
        session.setDate(new Date());
        session.setUserId(userId);
        WorkoutRoutineEntity routine = workoutRoutineRepository
                .findById(routineId)
                .orElseThrow(() -> new ResourceNotFoundException("Wrong workout routine!"));
        session.setRoutine(routine);

        session = workoutSessionRepository.save(session);
        var performedExercises = request.getExercises();
        Set<WorkoutSessionExerciseEntity> exerciseEntities = new HashSet<>();
        for (var performedExercise: performedExercises) {
            WorkoutExerciseRequestResponse exercise = performedExercise.getExercise();
            WorkoutExerciseEntity exerciseEntity = workoutExerciseRepository
                    .findById(exercise.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Wrong exercise!"));
            if (performedExercise.getGoals() != null) {
                for (var performedSet: performedExercise.getGoals()) {
                    if (performedSet.getPerformedReps() != null && performedSet.getPerformedWeight() != null) {
                        WorkoutSessionExerciseEntity workoutSessionExerciseEntity = new WorkoutSessionExerciseEntity();
                        workoutSessionExerciseEntity.setWorkoutSession(session);
                        workoutSessionExerciseEntity.setWorkoutExercise(exerciseEntity);
                        workoutSessionExerciseEntity.setSet(performedSet.getSet());
                        workoutSessionExerciseEntity.setReps(performedSet.getPerformedReps());
                        workoutSessionExerciseEntity.setWeight(performedSet.getPerformedWeight());
                        workoutSessionExerciseEntity.setNotes(performedSet.getNotes());
                        exerciseEntities.add(workoutSessionExerciseEntity);
                    }

                }
            }
        }
        session.setExercises(exerciseEntities);
        workoutSessionRepository.save(session);
    }

    public WorkoutSessionDetailsResponse getWorkoutSession(Integer userId, Integer sessionId) {
        WorkoutSessionEntity session = workoutSessionRepository.findById(sessionId)
                .orElseThrow(() -> new ResourceNotFoundException("Session does not exist"));
        return WorkoutSessionDetailsResponse.fromEntity(session);
    }
}
