package com.plachta.workoutservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "WorkoutExercise", schema = "public")
public class WorkoutExerciseEntity {
    private Integer id;
    //mozliwe ze trzeba bedzie zmienic na slownik cwiczen
    private String exerciseName;
    private Integer sets;
    private Integer minReps;
    private Integer maxReps;
    private WorkoutRoutineEntity workoutRoutineEntity;

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "exerciseName")
    @NotNull
    @NotBlank
    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    @Column(name = "sets")
    @NotNull
    public Integer getSets() {
        return sets;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    @Column(name = "minReps")
    @NotNull
    public Integer getMinReps() {
        return minReps;
    }

    public void setMinReps(Integer minReps) {
        this.minReps = minReps;
    }

    @Column(name = "maxReps")
    @NotNull
    public Integer getMaxReps() {
        return maxReps;
    }

    public void setMaxReps(Integer maxReps) {
        this.maxReps = maxReps;
    }

    @JoinColumn(name = "workoutRoutineId", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    public WorkoutRoutineEntity getWorkoutRoutineEntity() {
        return workoutRoutineEntity;
    }

    public void setWorkoutRoutineEntity(WorkoutRoutineEntity workoutRoutineEntity) {
        this.workoutRoutineEntity = workoutRoutineEntity;
    }
}
