package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutSessionExerciseEntity;

import java.math.BigDecimal;

public class SessionExerciseResponse {
    private String exerciseName;
    private BigDecimal weight;
    private Integer set;
    private Integer reps;
    private String notes;

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getSet() {
        return set;
    }

    public void setSet(Integer set) {
        this.set = set;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public static SessionExerciseResponse fromEntity(WorkoutSessionExerciseEntity entity) {
        SessionExerciseResponse response = new SessionExerciseResponse();
        response.setExerciseName(entity.getWorkoutExercise().getExerciseName());
        response.setReps(entity.getReps());
        response.setWeight(entity.getWeight());
        response.setSet(entity.getSet());
        response.setNotes(entity.getNotes());
        return response;
    }
}
