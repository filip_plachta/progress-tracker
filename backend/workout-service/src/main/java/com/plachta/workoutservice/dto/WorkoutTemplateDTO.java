package com.plachta.workoutservice.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

public class WorkoutTemplateDTO {

    @NotNull
    private List<ExerciseTemplateDTO> exercises;

    public List<ExerciseTemplateDTO> getExercises() {
        return exercises;
    }

    public void setExercises(List<ExerciseTemplateDTO> exercises) {
        this.exercises = exercises;
    }
}
