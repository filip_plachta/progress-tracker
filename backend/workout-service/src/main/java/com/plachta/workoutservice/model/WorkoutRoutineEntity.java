package com.plachta.workoutservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "workoutRoutine", schema = "public")
public class WorkoutRoutineEntity {
    private Integer owner;
    private String name;
    private Boolean isPrivate;
    private Integer id;
    private Date created;
    private Set<WorkoutExerciseEntity> exercises;

    @Column(name = "owner")
    @NotNull
    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    @Column(name = "name")
    @NotNull
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "isPrivate")
    @NotNull
    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "created")
    @NotNull
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "workoutRoutineEntity")
    public Set<WorkoutExerciseEntity> getExercises() {
        return exercises;
    }

    public void setExercises(Set<WorkoutExerciseEntity> exercises) {
        this.exercises = exercises;
    }
}
