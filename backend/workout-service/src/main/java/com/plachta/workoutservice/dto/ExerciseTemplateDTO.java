package com.plachta.workoutservice.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ExerciseTemplateDTO {
    @NotNull
    private WorkoutExerciseRequestResponse exercise;
    private List<ExerciseGoalDTO> goals;

    public WorkoutExerciseRequestResponse getExercise() {
        return exercise;
    }

    public void setExercise(WorkoutExerciseRequestResponse exercise) {
        this.exercise = exercise;
    }

    public List<ExerciseGoalDTO> getGoals() {
        return goals;
    }

    public void setGoals(List<ExerciseGoalDTO> goals) {
        this.goals = goals;
    }
}
