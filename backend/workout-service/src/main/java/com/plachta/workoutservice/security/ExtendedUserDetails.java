package com.plachta.workoutservice.security;

import com.plachta.workoutservice.dto.UserResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class ExtendedUserDetails implements UserDetails {
    private UserResponse user;

    public ExtendedUserDetails(final UserResponse user) {
        this.user = user;
    }

    public ExtendedUserDetails() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
       return Collections.EMPTY_LIST;
    }

    @Override
    public String getPassword() {
        return "";
    }
    @Override
    public String getUsername() {
        if (this.user == null) {
            return null;
        }
        return this.user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public UserResponse getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "CustomUserDetails [user=" + user + "]";
    }
}
