package com.plachta.workoutservice.dto;

import java.math.BigDecimal;

public class ExerciseGoalDTO {
    private Integer set;
    private BigDecimal weight;
    private Integer reps;

    private BigDecimal performedWeight;
    private Integer performedReps;
    private String notes;

    public Integer getSet() {
        return set;
    }

    public void setSet(Integer set) {
        this.set = set;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public BigDecimal getPerformedWeight() {
        return performedWeight;
    }

    public void setPerformedWeight(BigDecimal performedWeight) {
        this.performedWeight = performedWeight;
    }

    public Integer getPerformedReps() {
        return performedReps;
    }

    public void setPerformedReps(Integer performedReps) {
        this.performedReps = performedReps;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
