package com.plachta.workoutservice.controllers;

import com.plachta.workoutservice.dto.WorkoutSessionRequest;
import com.plachta.workoutservice.dto.WorkoutTemplateDTO;
import com.plachta.workoutservice.security.ExtendedUserDetails;
import com.plachta.workoutservice.service.WorkoutSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/session")
public class WorkoutSessionController {

    @Autowired
    private WorkoutSessionService workoutSessionService;

    @GetMapping
    public ResponseEntity<?> getMyWorkoutSessions(Authentication auth) {
        ExtendedUserDetails user = (ExtendedUserDetails) auth.getPrincipal();
        return ResponseEntity.ok(workoutSessionService.getUserWorkoutSessions(user.getUser().getId()));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getUserWorkoutSessions(@PathVariable Integer userId) {
        return ResponseEntity.ok(workoutSessionService.getUserWorkoutSessions(userId));
    }

    @GetMapping("/init/{routineId}")
    public ResponseEntity<?> initWorkoutSession(Authentication auth, @PathVariable Integer routineId) {
        ExtendedUserDetails user = (ExtendedUserDetails) auth.getPrincipal();
        return ResponseEntity.ok(workoutSessionService.createWorkoutTemplate(routineId, user.getUser().getId()));
    }

    @PostMapping("/{routineId}")
    public ResponseEntity<?> createWorkoutSession(Authentication auth, @PathVariable Integer routineId,
                                                  @RequestBody @Valid WorkoutTemplateDTO request) {
        ExtendedUserDetails user = (ExtendedUserDetails) auth.getPrincipal();
        workoutSessionService.saveWorkoutSession(user.getUser().getId(), routineId, request);
        return ResponseEntity.ok("Session has been ended!");
    }

    @GetMapping("/{sessionId}")
    public ResponseEntity<?> getWorkoutSession(Authentication auth, @PathVariable Integer sessionId) {
        ExtendedUserDetails user = (ExtendedUserDetails) auth.getPrincipal();
        return ResponseEntity.ok(workoutSessionService.getWorkoutSession(user.getUser().getId(), sessionId));
    }

}
