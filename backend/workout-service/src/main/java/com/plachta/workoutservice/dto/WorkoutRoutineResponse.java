package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutRoutineEntity;

import java.util.Date;

public class WorkoutRoutineResponse {
    String name;
    Boolean isPrivate;
    Integer id;
    Date created;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public static WorkoutRoutineResponse fromEntity(WorkoutRoutineEntity entity) {
        WorkoutRoutineResponse response = new WorkoutRoutineResponse();
        response.setId(entity.getId());
        response.setPrivate(entity.getPrivate());
        response.setName(entity.getName());
        response.setCreated(entity.getCreated());
        return response;
    }
}
