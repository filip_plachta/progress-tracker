package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutSessionEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WorkoutSessionDetailsResponse {
    private Integer id;
    private Date date;
    private String routine;
    private List<SessionExerciseResponse> exercises;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRoutine() {
        return routine;
    }

    public void setRoutine(String routine) {
        this.routine = routine;
    }

    public List<SessionExerciseResponse> getExercises() {
        return exercises;
    }

    public void setExercises(List<SessionExerciseResponse> exercises) {
        this.exercises = exercises;
    }

    public static WorkoutSessionDetailsResponse fromEntity(WorkoutSessionEntity entity) {
        WorkoutSessionDetailsResponse response = new WorkoutSessionDetailsResponse();
        response.setDate(entity.getDate());
        response.setId(entity.getId());
        response.setRoutine(entity.getRoutine().getName());
        var exercises = entity
                .getExercises()
                .stream()
                .map(SessionExerciseResponse::fromEntity)
                .collect(Collectors.toList());
        Collections.sort(exercises, (a, b) -> b.getReps().compareTo(a.getReps()));
        response.setExercises(exercises);
        return response;
    }
}
