package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutRoutineEntity;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.stream.Collectors;

public class WorkoutRoutineRequestResponse {
    private String name;
    private boolean isPrivate;
    private Set<WorkoutExerciseRequestResponse> exercises;

    @NotNull
    @NotBlank
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @NotNull
    @Valid
    public Set<WorkoutExerciseRequestResponse> getExercises() {
        return exercises;
    }

    public void setExercises(Set<WorkoutExerciseRequestResponse> exercises) {
        this.exercises = exercises;
    }

    public static WorkoutRoutineRequestResponse fromEntity(WorkoutRoutineEntity entity) {
        WorkoutRoutineRequestResponse response = new WorkoutRoutineRequestResponse();
        response.setName(entity.getName());
        response.setIsPrivate(entity.getPrivate());
        response.setExercises(entity.getExercises()
                .stream()
                .map(WorkoutExerciseRequestResponse::fromEntity)
                .collect(Collectors.toSet()));
        return response;
    }
}
