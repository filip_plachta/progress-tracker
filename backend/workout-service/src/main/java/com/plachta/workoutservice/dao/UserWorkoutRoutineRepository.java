package com.plachta.workoutservice.dao;

import com.plachta.workoutservice.model.UserWorkoutRoutineEntity;
import com.plachta.workoutservice.model.UserWorkoutRoutineId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserWorkoutRoutineRepository extends CrudRepository<UserWorkoutRoutineEntity, UserWorkoutRoutineId> {

    List<UserWorkoutRoutineEntity> findAllByUserWorkoutRoutineIdUserId(Integer userId);
}
