package com.plachta.workoutservice.dao;

import com.plachta.workoutservice.model.WorkoutExerciseEntity;
import org.springframework.data.repository.CrudRepository;

public interface WorkoutExerciseRepository extends CrudRepository<WorkoutExerciseEntity, Integer> {
}
