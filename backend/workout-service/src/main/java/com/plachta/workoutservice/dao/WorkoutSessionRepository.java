package com.plachta.workoutservice.dao;

import com.plachta.workoutservice.model.WorkoutSessionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.Set;

public interface WorkoutSessionRepository extends CrudRepository<WorkoutSessionEntity, Integer > {

    Set<WorkoutSessionEntity> findAllByUserIdOrderByDateDesc(Integer userId);

    Optional<WorkoutSessionEntity> findTopByRoutine_IdAndUserIdOrderByDateDesc(Integer routineId, Integer userId);
}
