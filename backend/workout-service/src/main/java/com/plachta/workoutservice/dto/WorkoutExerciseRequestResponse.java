package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutExerciseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class WorkoutExerciseRequestResponse {

    private Integer id;
    private String exerciseName;
    private Integer minReps;
    private Integer maxReps;
    private Integer sets;

    @NotNull
    @NotBlank
    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    @NotNull
    public Integer getMinReps() {
        return minReps;
    }

    public void setMinReps(Integer minReps) {
        this.minReps = minReps;
    }

    @NotNull
    public Integer getMaxReps() {
        return maxReps;
    }

    public void setMaxReps(Integer maxReps) {
        this.maxReps = maxReps;
    }

    public Integer getSets() {
        return sets;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static WorkoutExerciseRequestResponse fromEntity(WorkoutExerciseEntity entity) {
        WorkoutExerciseRequestResponse response = new WorkoutExerciseRequestResponse();
        response.setExerciseName(entity.getExerciseName());
        response.setMaxReps(entity.getMaxReps());
        response.setMinReps(entity.getMinReps());
        response.setSets(entity.getSets());
        response.setId(entity.getId());
        return response;

    }
}
