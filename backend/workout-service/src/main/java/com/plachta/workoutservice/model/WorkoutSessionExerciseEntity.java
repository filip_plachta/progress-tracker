package com.plachta.workoutservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "workoutSessionExercise")
public class WorkoutSessionExerciseEntity {
    private Integer id;
    private WorkoutExerciseEntity workoutExercise;
    private WorkoutSessionEntity workoutSession;
    private Integer set;
    private Integer reps;
    private String notes;
    private BigDecimal weight;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JoinColumn(name = "workoutExerciseId", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    public WorkoutExerciseEntity getWorkoutExercise() {
        return workoutExercise;
    }

    public void setWorkoutExercise(WorkoutExerciseEntity workoutExercise) {
        this.workoutExercise = workoutExercise;
    }

    @JoinColumn(name = "workoutSessionId", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    public WorkoutSessionEntity getWorkoutSession() {
        return workoutSession;
    }

    public void setWorkoutSession(WorkoutSessionEntity workoutSession) {
        this.workoutSession = workoutSession;
    }

    @NotNull
    @Column(name = "set")
    public Integer getSet() {
        return set;
    }

    public void setSet(Integer set) {
        this.set = set;
    }

    @Column(name = "reps")
    @NotNull
    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    @Column(name = "notes")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "weight")
    @NotNull
    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
