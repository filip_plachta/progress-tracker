package com.plachta.workoutservice.controllers;

import com.plachta.workoutservice.dto.WorkoutRoutineRequestResponse;
import com.plachta.workoutservice.security.ExtendedUserDetails;
import com.plachta.workoutservice.service.WorkoutRoutineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/routine")
public class WorkoutRoutineController {

    @Autowired
    private WorkoutRoutineService workoutRoutineService;

    @GetMapping
    public ResponseEntity<?> getMyWorkoutRoutines(Authentication auth) {
        ExtendedUserDetails userDetails = (ExtendedUserDetails) auth.getPrincipal();
        return ResponseEntity.ok(workoutRoutineService.getUserWorkoutRoutines(userDetails.getUser().getId(), false));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserWorkoutRoutines(@PathVariable Integer id) {
        // jak starczy czasu dodac weryfikacje czy logged user posiada ID w friends
        return ResponseEntity.ok(workoutRoutineService.getUserWorkoutRoutines(id, true));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> linkWithRoutine(Authentication auth, @PathVariable Integer id) {
        ExtendedUserDetails userDetails = (ExtendedUserDetails) auth.getPrincipal();
        workoutRoutineService.linkUserWithRoutine(userDetails.getUser().getId(), id);
        return ResponseEntity.ok("Routine has been added!");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> unlinkWithRoutine(Authentication auth, @PathVariable Integer id) {
        ExtendedUserDetails userDetails = (ExtendedUserDetails) auth.getPrincipal();
        workoutRoutineService.unlinkUserWithRoutine(userDetails.getUser().getId(), id);
        return ResponseEntity.ok("Routine has been deleted!");
    }

    @PostMapping
    public ResponseEntity<?> addUserWorkoutRoutine(Authentication auth,
                                                   @RequestBody @Valid WorkoutRoutineRequestResponse request) {
        ExtendedUserDetails userDetails = (ExtendedUserDetails) auth.getPrincipal();
        workoutRoutineService.addNewWorkoutRoutine(request, userDetails.getUser().getId());
        return ResponseEntity.ok("New workout routine has been added");
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<?> getRoutineDetails(Authentication auth, @PathVariable Integer id) {
        return ResponseEntity.ok(workoutRoutineService.getRoutineDetails(id));
    }
}
