package com.plachta.workoutservice.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("")
public class TestController {

    @GetMapping("/hello")
    public String hello(HttpServletRequest request) {
        return "hello from workout-service";
    }
}
