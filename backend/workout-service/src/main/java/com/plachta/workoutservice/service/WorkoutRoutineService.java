package com.plachta.workoutservice.service;

import com.plachta.workoutservice.dao.UserWorkoutRoutineRepository;
import com.plachta.workoutservice.dao.WorkoutRoutineRepository;
import com.plachta.workoutservice.dto.WorkoutExerciseRequestResponse;
import com.plachta.workoutservice.dto.WorkoutRoutineRequestResponse;
import com.plachta.workoutservice.dto.WorkoutRoutineResponse;
import com.plachta.workoutservice.exception.ResourceNotFoundException;
import com.plachta.workoutservice.model.UserWorkoutRoutineEntity;
import com.plachta.workoutservice.model.UserWorkoutRoutineId;
import com.plachta.workoutservice.model.WorkoutExerciseEntity;
import com.plachta.workoutservice.model.WorkoutRoutineEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class WorkoutRoutineService {

    @Autowired
    private WorkoutRoutineRepository workoutRoutineRepository;
    @Autowired
    private UserWorkoutRoutineRepository userWorkoutRoutineRepository;

    @Transactional
    public List<WorkoutRoutineResponse> getUserWorkoutRoutines(Integer userId, boolean onlyPublic) {
        List<UserWorkoutRoutineEntity> userWorkouts = userWorkoutRoutineRepository
                .findAllByUserWorkoutRoutineIdUserId(userId);

        List<WorkoutRoutineEntity> workouts = userWorkouts
                .stream()
                .map((UserWorkoutRoutineEntity userWorkout) -> workoutRoutineRepository
                        .findById(userWorkout.getUserWorkoutRoutineId().getWorkoutRoutineId()).get())
                .collect(Collectors.toList());

        if (onlyPublic) {
            workouts = workouts
                    .stream()
                    .filter((WorkoutRoutineEntity e) -> {
                        return !e.getPrivate();
                    })
                    .collect(Collectors.toList());
        }

        return workouts
                .stream()
                .map(WorkoutRoutineResponse::fromEntity)
                .collect(Collectors.toList());
    }

    public void addNewWorkoutRoutine(WorkoutRoutineRequestResponse request, Integer userId) {
        WorkoutRoutineEntity routine = new WorkoutRoutineEntity();
        routine.setName(request.getName());
        routine.setPrivate(request.getIsPrivate());
        routine.setCreated(new Date());
        routine.setOwner(userId);
        final var newRoutine = workoutRoutineRepository.save(routine);
        Set<WorkoutExerciseEntity> exercises = request.getExercises().stream().map((WorkoutExerciseRequestResponse exerciseRequest) -> {
            WorkoutExerciseEntity entity = new WorkoutExerciseEntity();
            entity.setExerciseName(exerciseRequest.getExerciseName());
            entity.setMaxReps(exerciseRequest.getMaxReps());
            entity.setMinReps(exerciseRequest.getMinReps());
            entity.setSets(exerciseRequest.getSets());
            entity.setWorkoutRoutineEntity(newRoutine);
            return entity;
        }).collect(Collectors.toSet());
        newRoutine.setExercises(exercises);
        workoutRoutineRepository.save(routine);

        UserWorkoutRoutineEntity userWorkoutRoutineEntity = new UserWorkoutRoutineEntity();
        UserWorkoutRoutineId id = new UserWorkoutRoutineId();
        id.setUserId(userId);
        id.setWorkoutRoutineId(newRoutine.getId());
        userWorkoutRoutineEntity.setUserWorkoutRoutineId(id);
        userWorkoutRoutineRepository.save(userWorkoutRoutineEntity);
    }

    public void linkUserWithRoutine(Integer userId, Integer routineId) {
        if (!workoutRoutineRepository.existsById(routineId)) {
            throw new ResourceNotFoundException("Routine does not exist!");
        }
        UserWorkoutRoutineId id = new UserWorkoutRoutineId();
        id.setUserId(userId);
        id.setWorkoutRoutineId(routineId);
        UserWorkoutRoutineEntity entity = new UserWorkoutRoutineEntity();
        entity.setUserWorkoutRoutineId(id);
        userWorkoutRoutineRepository.save(entity);
    }

    public void unlinkUserWithRoutine(Integer userId, Integer routineId) {
        if (!workoutRoutineRepository.existsById(routineId)) {
            throw new ResourceNotFoundException("Routine does not exist!");
        }

        UserWorkoutRoutineId id = new UserWorkoutRoutineId();
        id.setUserId(userId);
        id.setWorkoutRoutineId(routineId);
        userWorkoutRoutineRepository.deleteById(id);
    }

    public WorkoutRoutineRequestResponse getRoutineDetails(Integer routineId) {
        WorkoutRoutineEntity entity = workoutRoutineRepository
                .findById(routineId)
                .orElseThrow(() -> new ResourceNotFoundException("Workout routine does not exist"));
        return WorkoutRoutineRequestResponse.fromEntity(entity);
    }
}
