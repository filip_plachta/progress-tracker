package com.plachta.workoutservice.dto;

import com.plachta.workoutservice.model.WorkoutExerciseEntity;
import com.plachta.workoutservice.model.WorkoutSessionEntity;

import java.util.Date;

public class WorkoutSessionResponse {
    private Integer id;
    private Date date;
    private String routine;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setRoutine(String routine) {
        this.routine = routine;
    }

    public String getRoutine() {
        return routine;
    }

    public static WorkoutSessionResponse fromEntity(WorkoutSessionEntity entity) {
        WorkoutSessionResponse response = new WorkoutSessionResponse();
        response.setId(entity.getId());
        response.setDate(entity.getDate());
        response.setRoutine(entity.getRoutine().getName());
        return response;
    }
}
