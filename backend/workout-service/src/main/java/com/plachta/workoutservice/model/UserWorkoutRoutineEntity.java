package com.plachta.workoutservice.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="UserWorkoutRoutine", schema = "public")
public class UserWorkoutRoutineEntity implements Serializable {
    @EmbeddedId
    private UserWorkoutRoutineId userWorkoutRoutineId;

    public UserWorkoutRoutineId getUserWorkoutRoutineId() {
        return userWorkoutRoutineId;
    }

    public void setUserWorkoutRoutineId(UserWorkoutRoutineId userWorkoutRoutineId) {
        this.userWorkoutRoutineId = userWorkoutRoutineId;
    }
}
