package com.plachta.workoutservice.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "workoutSession", schema = "public")
public class WorkoutSessionEntity {
    private Integer id;
    private Date date;
    private Integer userId;
    private WorkoutRoutineEntity routine;
    private Set<WorkoutSessionExerciseEntity> exercises;

    @Id
    @Column(name = "id")
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "date")
    @NotNull
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "userId")
    @NotNull
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer login) {
        this.userId = login;
    }

    @NotNull
    @JoinColumn(name = "routineId", referencedColumnName = "id")
    @ManyToOne
    public WorkoutRoutineEntity getRoutine() {
        return routine;
    }

    public void setRoutine(WorkoutRoutineEntity routine) {
        this.routine = routine;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "workoutSession")
    public Set<WorkoutSessionExerciseEntity> getExercises() {
        return exercises;
    }

    public void setExercises(Set<WorkoutSessionExerciseEntity> exercises) {
        this.exercises = exercises;
    }
}
