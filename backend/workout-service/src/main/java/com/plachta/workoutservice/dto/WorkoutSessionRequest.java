package com.plachta.workoutservice.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class WorkoutSessionRequest {

    List<SessionExerciseRequestResponse> exercises;

    @NotNull
    @Valid
    public List<SessionExerciseRequestResponse> getExercises() {
        return exercises;
    }

    public void setExercises(List<SessionExerciseRequestResponse> exercises) {
        this.exercises = exercises;
    }
}
