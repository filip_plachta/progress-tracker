package com.plachta.workoutservice.dao;

import com.plachta.workoutservice.model.WorkoutRoutineEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WorkoutRoutineRepository extends CrudRepository<WorkoutRoutineEntity, Integer> {
    List<WorkoutRoutineEntity> findAll();
}
