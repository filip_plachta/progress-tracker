package com.plachta.workoutservice.dto;

import java.math.BigDecimal;

public class SessionExerciseRequestResponse {
    private WorkoutExerciseRequestResponse exercise;
    private Integer reps;
    private Integer set;
    private BigDecimal weight;
    private String notes;

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public Integer getSet() {
        return set;
    }

    public void setSet(Integer set) {
        this.set = set;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WorkoutExerciseRequestResponse getExercise() {
        return exercise;
    }

    public void setExercise(WorkoutExerciseRequestResponse exercise) {
        this.exercise = exercise;
    }
}
