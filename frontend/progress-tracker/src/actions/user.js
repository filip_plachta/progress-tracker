import { axiosDefault } from "./axios";

function register(data) {
  return axiosDefault.post("/users/user/registration", data);
}

function getUserData() {
  return axiosDefault.get("/users/user/me");
}

function getUser(id) {
  return axiosDefault.get(`/users/user/${id}`);
}

export { register, getUserData, getUser };
