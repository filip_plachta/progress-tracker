import { axiosDefault } from "./axios";

function getMyRoutines() {
  return axiosDefault.get("/workout/routine");
}

function addRoutine(data) {
  return axiosDefault.post("/workout/routine", data);
}

function getUserRoutines(id) {
  return axiosDefault.get(`/workout/routine/${id}`);
}

function linkWithRoutine(id) {
  return axiosDefault.post(`/workout/routine/${id}`);
}

function getRoutine(id) {
  return axiosDefault.get(`/workout/routine/details/${id}`);
}

function unlinkRoutine(id) {
  return axiosDefault.delete(`/workout/routine/${id}`);
}

export {
  getMyRoutines,
  addRoutine,
  getUserRoutines,
  linkWithRoutine,
  getRoutine,
  unlinkRoutine,
};
