import axios from "axios";

const axiosDefault = axios.create({
  baseURL: "http://104.248.47.147:8080",
  timeout: 1000,
});

export { axiosDefault };
