import { axiosDefault } from "./axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

function auth(data) {
  delete axiosDefault.defaults.headers.common["Authorization"];
  return axiosDefault.post("/users/auth", data);
}

async function setToken(token) {
  axiosDefault.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  await AsyncStorage.setItem("Token", `Bearer ${token}`);
}

export { auth, setToken };
