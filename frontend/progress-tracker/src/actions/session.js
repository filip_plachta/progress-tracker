import { axiosDefault } from "./axios";

function getMySessions() {
  return axiosDefault.get("/workout/session");
}

function initSession(routineId) {
  return axiosDefault.get(`/workout/session/init/${routineId}`);
}

function addSession(routineId, data) {
  return axiosDefault.post(`/workout/session/${routineId}`, data);
}

function getSession(sessionId) {
  return axiosDefault.get(`/workout/session/${sessionId}`);
}

function getUserSessions(userId) {
  return axiosDefault.get(`/workout/session/user/${userId}`);
}

export { getMySessions, initSession, addSession, getSession, getUserSessions };
