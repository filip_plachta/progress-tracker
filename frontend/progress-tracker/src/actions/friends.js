import { axiosDefault } from "./axios";

function getMyFriends() {
  return axiosDefault.get("/users/friends");
}

function deleteFriendship(id) {
  return axiosDefault.delete(`/users/friends/${id}`);
}

function addFriend(data) {
  return axiosDefault.post("/users/friends", data);
}

function getFriendsRequests() {
  return axiosDefault.get("/users/friends/requests");
}

function acceptFriend(data) {
  return axiosDefault.put("/users/friends/accept", data);
}

function declineFriend(data) {
  return axiosDefault.put("/users/friends/decline", data);
}

export {
  getMyFriends,
  deleteFriendship,
  addFriend,
  getFriendsRequests,
  acceptFriend,
  declineFriend,
};
