import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import { createDrawerNavigator } from "react-navigation-drawer";
import HomeScreen from "../screens/home/HomeScreen";
import RoutineScreen from "../screens/routine/RoutineScreen";
import AddRoutineScreen from "../screens/routine/details/AddRoutineScreen";
import AppHeader from "../components/AppHeader";
import React from "react";
import { View } from "react-native";
import AppDrawer from "../components/AppDrawer";
import SessionScreen from "../screens/session/SessionScreen";
import FriendsScreen from "../screens/friends/FriendsScreen";
import FriendRequestsScreen from "../screens/friends/requests/FriendRequestsScreen";
import UserProfileScreen from "../screens/user/UserProfileScreen";
import AddSessionScreen from "../screens/session/add/AddSessionScreen";
import SessionDetailsScreen from "../screens/session/details/SessionDetailsScreen";

const home = {
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: "Home",
    },
  },
};

const routine = {
  Routine: {
    screen: RoutineScreen,
    navigationOptions: {
      title: "Workout routines",
    },
  },
  AddRoutine: {
    screen: AddRoutineScreen,
    navigationOptions: {
      title: "Workout routine details",
    },
  },
};

const session = {
  Session: {
    screen: SessionScreen,
    navigationOptions: {
      title: "Workout sessions",
    },
  },
  AddSession: {
    screen: AddSessionScreen,
    navigationOptions: {
      title: "Add workout session",
    },
  },
  SessionDetails: {
    screen: SessionDetailsScreen,
    navigationOptions: {
      title: "Workout session details",
    },
  },
};

const friends = {
  Friends: {
    screen: FriendsScreen,
    navigationOptions: {
      title: "Friends",
    },
  },
  FriendRequests: {
    screen: FriendRequestsScreen,
    navigationOptions: {
      title: "Friend requests",
    },
  },
  UserProfile: {
    screen: UserProfileScreen,
    navigationOptions: {
      title: "User profile",
    },
  },
  UserRoutines: {
    screen: RoutineScreen,
    navigationOptions: {
      title: "User routines",
    },
  },
  UserSessions: {
    screen: SessionScreen,
    navigationOptions: {
      title: "User sessions",
    },
  },
  UserRoutinesDetails: {
    screen: AddRoutineScreen,
    navigationOptions: {
      title: "User workout routine",
    },
  },
  UserSessionDetails: {
    screen: SessionDetailsScreen,
    navigationOptions: {
      title: "User session details",
    },
  },
};
const defaultNavigationOptions = {
  // eslint-disable-next-line react/display-name
  cardOverlay: () => (
    <View
      style={{
        flex: 1,
        backgroundColor: "#121212",
      }}
    />
  ),
  // eslint-disable-next-line react/display-name
  header: ({ scene, previous, navigation }) => (
    <AppHeader scene={scene} previous={previous} navigation={navigation} />
  ),
};

const HomeStack = createStackNavigator(home, {
  mode: "modal",
  defaultNavigationOptions,
});
const RoutineStack = createStackNavigator(routine, {
  mode: "modal",
  defaultNavigationOptions,
});
const SessionStack = createStackNavigator(session, {
  mode: "modal",
  defaultNavigationOptions,
});
const FriendsStack = createStackNavigator(friends, {
  mode: "modal",
  defaultNavigationOptions,
});

const Drawer = createDrawerNavigator(
  {
    HomeModule: { screen: HomeStack },
    RoutineModule: { screen: RoutineStack },
    SessionModule: { screen: SessionStack },
    FriendsModule: { screen: FriendsStack },
  },
  {
    // eslint-disable-next-line react/display-name
    contentComponent: (props) => <AppDrawer {...props} />,
  }
);

export default createAppContainer(Drawer);
