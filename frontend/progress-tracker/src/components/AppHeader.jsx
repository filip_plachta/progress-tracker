import React from "react";
import { Appbar, ThemeProvider } from "react-native-paper";
import { StatusBar, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

function AppHeader({ scene, previous, navigation }) {
  const { title } = scene.descriptor.options;

  return (
    <SafeAreaView>
      <StatusBar style={{ color: "white" }}></StatusBar>
      <Appbar>
        {previous ? (
          <Appbar.BackAction
            onPress={() => {
              navigation.goBack();
            }}
          />
        ) : (
          <Appbar.Action icon="menu" onPress={() => navigation.openDrawer()} />
        )}

        <Appbar.Content title={title} />
      </Appbar>
    </SafeAreaView>
  );
}

export default AppHeader;
