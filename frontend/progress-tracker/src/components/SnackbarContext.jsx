/* eslint-disable react/display-name */
import React from "react";
import { Button, Snackbar, withTheme } from "react-native-paper";

const def_state = {
  open: false,
  message: "",
};

const SnackbarContext = React.createContext(def_state);

class SnackbarProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, def_state);
    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  handleOpen(message) {
    this.setState({
      open: true,
      message,
    });
  }

  handleClose() {
    this.setState({
      open: false,
      message: "",
    });
  }

  render() {
    const { children } = this.props;
    const { theme } = this.props;
    return (
      <>
        <Snackbar
          visible={this.state.open}
          onDismiss={this.handleClose}
          duration={6000}
          theme={theme}
        >
          {this.state.message}
        </Snackbar>
        <SnackbarContext.Provider
          value={{
            ...this.state,
            handleOpen: this.handleOpen,
          }}
        >
          {children}
        </SnackbarContext.Provider>
      </>
    );
  }
}

const SnackbarConsumer = SnackbarContext.Consumer;

const withSnackbar = (Component) => (props) => (
  <SnackbarConsumer>
    {({ message, handleOpen, open }) => (
      <Component {...props} showMessage={handleOpen} />
    )}
  </SnackbarConsumer>
);

export default withTheme(SnackbarProvider);
export { SnackbarContext, SnackbarConsumer, withSnackbar };
