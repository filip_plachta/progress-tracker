import React from "react";
import { TextInput, HelperText } from "react-native-paper";

function ValidationTextInput(props) {
  return (
    <>
      <TextInput
        onChangeText={props.handleChange}
        onBlur={props.handleChange}
        value={props.value}
        style={props.style}
        label={props.label}
        {...props}
      />
      {props.touched && props.errors && (
        <HelperText
          padding="none"
          type="error"
          visible={props.touched && props.errors}
        >
          {props.errors}
        </HelperText>
      )}
    </>
  );
}

export default ValidationTextInput;
