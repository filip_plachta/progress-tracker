import React from "react";
import { ActivityIndicator, withTheme } from "react-native-paper";
import { View, StyleSheet } from "react-native";

function SplashScreen({ theme }) {
  const { colors } = theme;
  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      <ActivityIndicator
        size={50}
        animating={true}
        color={colors.primary}
        style={{ flex: 1, flexDirection: "column" }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default withTheme(SplashScreen);
