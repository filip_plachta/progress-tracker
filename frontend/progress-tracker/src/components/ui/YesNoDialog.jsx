import React from "react";
import { Dialog, Paragraph, Button } from "react-native-paper";

function YesNoDialog(props) {
  return (
    <Dialog visible={props.visible} onDismiss={props.onClose}>
      <Dialog.Title>{props.title}</Dialog.Title>
      <Dialog.Content>
        <Paragraph>{props.content}</Paragraph>
      </Dialog.Content>
      <Dialog.Actions>
        <Button onPress={props.yesAction}>Yes</Button>
        <Button onPress={props.noAction}>No</Button>
      </Dialog.Actions>
    </Dialog>
  );
}

export default YesNoDialog;
