import React from "react";
import { ScrollView, View, StyleSheet } from "react-native";
import { Drawer, Headline, Caption, withTheme } from "react-native-paper";
import { AuthContext } from "./AuthContext";

function AppDrawer({ navigation, theme }) {
  let activeRoute =
    navigation?.state?.routes[navigation?.state?.index]?.routeName;
  const { userData, signOut } = React.useContext(AuthContext);
  const user = userData();
  return (
    <ScrollView
      contentContainerStyle={{
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "flex-start",
      }}
    >
      <View style={styles.infoSection}>
        <Headline style={{ color: theme.colors.primary }}>
          {user?.login}
        </Headline>
        <Caption>{user?.email}</Caption>
      </View>
      <Drawer.Section style={styles.drawerSection} />
      <Drawer.Section style={styles.drawerSection}>
        <Drawer.Item
          icon="home"
          label="Home"
          active={activeRoute === "HomeModule"}
          onPress={() => {
            navigation.navigate("HomeModule");
          }}
        />
        <Drawer.Item
          icon="dns"
          label="Workout routines"
          active={activeRoute === "RoutineModule"}
          onPress={() => {
            navigation.navigate("RoutineModule");
          }}
        />
        <Drawer.Item
          icon="alarm"
          label="Workout sessions"
          active={activeRoute === "SessionModule"}
          onPress={() => {
            navigation.navigate("SessionModule");
          }}
        />
        <Drawer.Item
          icon="account"
          label="Friends"
          active={activeRoute === "FriendsModule"}
          onPress={() => {
            navigation.navigate("FriendsModule");
          }}
        />
      </Drawer.Section>
      <Drawer.Item
        icon="close"
        label="Sign out"
        onPress={() => {
          signOut();
        }}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  infoSection: {
    marginTop: 15,
    marginLeft: 20,
  },
  drawerSection: {
    width: "100%",
  },
});

export default withTheme(AppDrawer);
