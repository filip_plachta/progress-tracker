import React from "react";
import { View, ScrollableView, StyleSheet } from "react-native";
import {
  Headline,
  Paragraph,
  Button,
  Divider,
  withTheme,
  Card,
} from "react-native-paper";

function UserComponent(props) {
  const { colors } = props.theme;
  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      <Headline style={{ color: colors.primary }}>
        {props?.user?.login}
      </Headline>
      <Paragraph>{props?.user.email}</Paragraph>
      <Divider />
      <View>
        <Button
          onPress={() => props.goToRoutines(props.user?.id)}
          style={styles.btn}
          mode="outlined"
        >
          Workout routines
        </Button>
        <Button
          style={styles.btn}
          mode="outlined"
          onPress={() => props.goToSessions(props.user?.id)}
        >
          Workout sessions
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  btn: {
    marginTop: "5%",
  },
});

export default withTheme(UserComponent);
