import React from "react";
import UserComponent from "./UserComponent";
import { NavigationEvents } from "react-navigation";
import { getUser } from "../../actions/user";
import SplashScreen from "../../components/ui/SplashScreen";

export default function UserProfileScreen(props) {
  const [user, setUser] = React.useState({});
  const [loading, setLoading] = React.useState(true);

  const fetchData = () => {
    const id = props.navigation.getParam("userId");
    getUser(id).then((res) => {
      setUser(res.data);
      setLoading(false);
    });
  };

  const goToRoutines = (userId) => {
    props.navigation.navigate("UserRoutines", { userId });
  };

  const goToSessions = (userId) => {
    props.navigation.navigate("UserSessions", { userId });
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      {loading ? (
        <SplashScreen loading={loading} />
      ) : (
        <UserComponent
          user={user}
          goToRoutines={goToRoutines}
          goToSessions={goToSessions}
        />
      )}
    </>
  );
}
