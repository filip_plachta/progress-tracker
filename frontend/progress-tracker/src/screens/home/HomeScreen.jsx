import React from "react";
import { SafeAreaView } from "react-navigation";
import { Text } from "react-native";

function HomeScreen() {
  return (
    <SafeAreaView>
      <Text style={{ color: "white" }}>HOME</Text>
    </SafeAreaView>
  );
}

export default HomeScreen;
