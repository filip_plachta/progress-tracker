import React from "react";
import { StyleSheet, View, TextInput } from "react-native";
import { withTheme, Button } from "react-native-paper";
import ValidationTextInput from "../../components/ui/ValidationTextInput";
import { withFormik } from "formik";
import * as Yup from "yup";
import { register } from "../../actions/user";
import { withSnackbar } from "../../components/SnackbarContext";

const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: (props) => ({
    email: "",
    login: "",
    password: "",
    passwordRepeat: "",
  }),

  handleSubmit: (values, { props }) => {
    delete values.passwordRepeat;
    register(values).then((response) => {
      props.showMessage(response?.data);
      props.navigation.navigate("Login");
    });
  },
  validationSchema: Yup.object().shape({
    email: Yup.string().required("E-mail is required"),
    login: Yup.string().required("Login is required"),
    password: Yup.string().required("Password is required"),
    passwordRepeat: Yup.string().required("Repeated password is required"),
  }),
});

function RegisterComponent(props) {
  const { colors } = props.theme;
  const { handleChange, handleBlur } = props;
  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      <ValidationTextInput
        onChangeText={handleChange("email")}
        onBlur={handleBlur("email")}
        value={props.values.email}
        style={styles.textInput}
        touched={props.touched.email}
        errors={props.errors.email}
        label="E-mail"
      />

      <ValidationTextInput
        onChangeText={handleChange("login")}
        onBlur={handleBlur("login")}
        value={props.values.login}
        style={styles.textInput}
        touched={props.touched.login}
        errors={props.errors.login}
        label="Login"
      />

      <ValidationTextInput
        onChangeText={handleChange("password")}
        onBlur={handleBlur("password")}
        value={props.values.password}
        style={styles.textInput}
        touched={props.touched.password}
        errors={props.errors.password}
        label="Password"
        secureTextEntry={true}
      />

      <ValidationTextInput
        onChangeText={handleChange("passwordRepeat")}
        onBlur={handleBlur("passwordRepeat")}
        value={props.values.passwordRepeat}
        style={styles.textInput}
        touched={props.touched.passwordRepeat}
        errors={props.errors.passwordRepeat}
        label="Repeat password"
        secureTextEntry={true}
      />

      <Button onPress={props.handleSubmit} style={styles.btn} mode="contained">
        Sign up
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  textInput: { marginTop: "4%" },
  btn: { marginTop: "10%" },
});

export default withSnackbar(withTheme(formikEnhancer(RegisterComponent)));
