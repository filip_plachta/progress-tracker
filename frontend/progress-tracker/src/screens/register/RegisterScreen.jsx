import React from "react";
import { Paragraph } from "react-native-paper";
import RegisterComponent from "./RegisterComponent";

function RegisterScreen(props) {
  return <RegisterComponent navigation={props.navigation} />;
}

export default RegisterScreen;
