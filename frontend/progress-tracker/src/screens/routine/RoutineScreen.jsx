import React, { useEffect } from "react";
import {
  getMyRoutines,
  getUserRoutines,
  linkWithRoutine,
  unlinkRoutine,
} from "../../actions/routine";
import RoutineComponent from "./RoutineComponent";
import { NavigationEvents, SafeAreaView } from "react-navigation";
import { withSnackbar } from "../../components/SnackbarContext";
import SplashScreen from "../../components/ui/SplashScreen";

function RoutineScreen(props) {
  const [routines, setRoutines] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [fabOpen, setFabOpen] = React.useState(false);
  const [mode, setMode] = React.useState("my");

  const fetchData = () => {
    const userId = props.navigation.getParam("userId");
    if (userId) {
      getUserRoutines(userId).then((response) => {
        setRoutines(response.data);
        setMode("user");
        setLoading(false);
      });
    } else {
      getMyRoutines().then((response) => {
        setRoutines(response.data);
        setLoading(false);
      });
    }
  };

  const onAdd = (routineId) => {
    if (mode === "user") {
      props.navigation.navigate("UserRoutinesDetails", {
        routineId,
      });
    } else {
      props.navigation.navigate("AddRoutine", { routineId });
    }
  };

  const onAddUserPlan = (id) => {
    linkWithRoutine(id)
      .then((res) => {
        props.showMessage(res.data);
      })
      .catch((error) => {
        props.showMessage(error.response?.data?.message);
      });
  };

  const onAddExisting = () => {
    props.navigation.navigate("FriendsModule");
  };

  const deleteRoutine = (id) => {
    unlinkRoutine(id)
      .then((res) => {
        props.showMessage(res.data);
        fetchData();
      })
      .catch((error) => {
        props.showMessage(error.response?.data?.message);
        fetchData();
      });
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      {loading ? (
        <SplashScreen />
      ) : (
        <RoutineComponent
          routines={routines}
          onAdd={onAdd}
          fabOpen={fabOpen}
          setFabOpen={setFabOpen}
          mode={mode}
          onAddUserPlan={onAddUserPlan}
          onAddExisting={onAddExisting}
          deleteRoutine={deleteRoutine}
        />
      )}
    </>
  );
}

export default withSnackbar(RoutineScreen);
