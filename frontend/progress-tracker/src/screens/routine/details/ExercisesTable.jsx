import React from "react";
import { View } from "react-native";
import { DataTable, IconButton } from "react-native-paper";

function ExercisesTable(props) {
  return (
    <View>
      <DataTable>
        <DataTable.Header>
          {props.editMode && (
            <DataTable.Title style={{ flex: 1.5 }}>Actions</DataTable.Title>
          )}

          <DataTable.Title>Exercise</DataTable.Title>
          <DataTable.Title>Reps</DataTable.Title>
          <DataTable.Title>Sets</DataTable.Title>
        </DataTable.Header>
        {props.exercises.map((exercise, index) => {
          return (
            <DataTable.Row key={index}>
              {props.editMode && (
                <DataTable.Cell style={{ flex: 1.5 }}>
                  <IconButton
                    onPress={() => props.removeExercise(index)}
                    icon="delete"
                  />
                </DataTable.Cell>
              )}

              <DataTable.Cell>{exercise?.exerciseName}</DataTable.Cell>
              <DataTable.Cell>
                {exercise?.minReps} - {exercise?.maxReps}
              </DataTable.Cell>
              <DataTable.Cell>{exercise?.sets}</DataTable.Cell>
            </DataTable.Row>
          );
        })}
      </DataTable>
    </View>
  );
}

export default ExercisesTable;
