import React from "react";
import { addRoutine, getRoutine } from "../../../actions/routine";
import AddRoutineComponent from "./AddRoutineComponent";
import { withSnackbar } from "../../../components/SnackbarContext";

class AddRoutineScreen extends React.Component {
  constructor(props) {
    super(props);
    const routineId = props.navigation.getParam("routineId");
    this.state = {
      exercises: [],
      dialogVisible: false,
      routineId,
      editMode: !routineId,
      routine: {},
    };
  }

  componentDidMount() {
    if (!this.state.editMode) {
      getRoutine(this.state.routineId).then((res) => {
        this.setState({
          routine: { name: res?.data?.name, isPrivate: res?.data?.isPrivate },
          exercises: res.data.exercises,
        });
      });
    }
  }

  showDialog = () => {
    this.setState({
      dialogVisible: true,
    });
  };

  hideDialog = () => {
    this.setState({
      dialogVisible: false,
    });
  };

  addExercise = (values) => {
    let newExercises = this.state.exercises;
    newExercises.push(values);
    this.setState({
      exercises: newExercises,
    });
  };

  removeExercise = (index) => {
    let newExercises = this.state.exercises;
    newExercises.splice(index, 1);
    this.setState({
      exercises: newExercises,
    });
  };

  onSubmit = (data) => {
    const dto = {
      ...data,
      exercises: this.state.exercises.map((exercise) => ({
        exerciseName: exercise.exerciseName,
        minReps: parseInt(exercise.minReps, 10),
        maxReps: parseInt(exercise.maxReps, 10),
        sets: parseInt(exercise.sets, 10),
      })),
    };
    addRoutine(dto).then((response) => {
      this.props.showMessage(response.data);
      this.props.navigation.pop();
    });
  };

  render() {
    return (
      <AddRoutineComponent
        exercises={this.state.exercises}
        removeExercise={this.removeExercise}
        addExercise={this.addExercise}
        showDialog={this.showDialog}
        hideDialog={this.hideDialog}
        visible={this.state.dialogVisible}
        onSubmit={this.onSubmit}
        editMode={this.state.editMode}
        routine={this.state.routine}
      />
    );
  }
}

export default withSnackbar(AddRoutineScreen);
