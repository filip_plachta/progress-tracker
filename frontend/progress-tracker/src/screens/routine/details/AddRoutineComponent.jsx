import React from "react";
import { ScrollView, StyleSheet } from "react-native";
import { withTheme, Checkbox, Button } from "react-native-paper";
import ValidationTextInput from "../../../components/ui/ValidationTextInput";
import ExercisesTable from "./ExercisesTable";
import { withFormik } from "formik";
import * as Yup from "yup";
import AddExerciseDialog from "./AddExerciseDialog";

const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: (props) => ({
    name: !props.editMode && props.routine?.name ? props.routine.name : "",
    isPrivate: false,
  }),

  handleSubmit: (values, { props }) => {
    props.onSubmit(values);
  },
  validationSchema: Yup.object().shape({
    name: Yup.string().required("Name is required"),
  }),
});

function AddRoutineComponent(props) {
  const { handleChange, handleBlur, handleSubmit } = props;
  return (
    <ScrollView
      style={[
        styles.container,
        { backgroundColor: props.theme.colors.surface },
      ]}
    >
      <ValidationTextInput
        onChangeText={handleChange("name")}
        onBlur={handleBlur("name")}
        value={props.values.name}
        style={styles.textInput}
        touched={props.touched.name}
        errors={props.errors.name}
        label="Name"
        disabled={!props.editMode}
      />

      <Checkbox.Item
        style={styles.textInput}
        label="Private"
        status={props.values.isPrivate ? "checked" : "unchecked"}
        onPress={() => {
          if (props.editMode) {
            props.setFieldValue("isPrivate", !props.values.isPrivate);
          }
        }}
        disabled={!props.editMode}
      >
        Private
      </Checkbox.Item>
      <ExercisesTable
        removeExercise={props.removeExercise}
        exercises={props.exercises}
        editMode={props.editMode}
      />
      {props.editMode && (
        <>
          <Button onPress={props.showDialog} style={styles.btn}>
            Add exercise
          </Button>
          <AddExerciseDialog
            handleSubmit={props.addExercise}
            visible={props.visible}
            hide={props.hideDialog}
          />
          <Button mode="contained" onPress={handleSubmit} style={styles.submit}>
            Add workout routine
          </Button>
        </>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  textInput: { marginTop: "4%" },
  btn: { marginTop: "10%", alignSelf: "flex-end" },
  submit: { position: "absolute", bottom: 5 },
});

export default withTheme(formikEnhancer(AddRoutineComponent));
