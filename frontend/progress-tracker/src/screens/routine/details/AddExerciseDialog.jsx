import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Button, Dialog, Portal, Divider, withTheme } from "react-native-paper";
import { withFormik } from "formik";
import * as Yup from "yup";
import ValidationTextInput from "../../../components/ui/ValidationTextInput";

const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: (props) => ({
    exerciseName: "",
    minReps: "",
    maxReps: "",
    sets: "",
  }),

  handleSubmit: (values, { props }) => {
    props.hide();
    props.handleSubmit(values);
  },

  validationSchema: Yup.object().shape({
    exerciseName: Yup.string().required("Exercise is required"),
    minReps: Yup.number().required("required"),
    maxReps: Yup.number().required("required"),
    sets: Yup.string().required("Sets are required"),
  }),
});

function AddExerciseDialog(props) {
  const { handleChange, handleBlur } = props;
  return (
    <Portal>
      <Dialog visible={props.visible} onDismiss={props.hide}>
        <Dialog.Title>Add exercise</Dialog.Title>
        <Divider />
        <Dialog.Content>
          <ValidationTextInput
            onChangeText={handleChange("exerciseName")}
            onBlur={handleBlur("exerciseName")}
            value={props.values.exerciseName}
            style={styles.textInput}
            // touched={props.touched.exercise}
            // errors={props.errors.exercise}
            label="Exercise"
          />
          <View
            style={{
              flexWrap: "wrap",
              alignItems: "flex-start",
              flexDirection: "row",
              height: "auto",
            }}
          >
            <ValidationTextInput
              onChangeText={handleChange("minReps")}
              onBlur={handleBlur("minReps")}
              value={props.values.minReps}
              style={styles.minReps}
              // touched={props.touched.minReps}
              // errors={props.errors.minReps}
              label="Min reps"
            />

            <ValidationTextInput
              onChangeText={handleChange("maxReps")}
              onBlur={handleBlur("maxReps")}
              value={props.values.maxReps}
              style={styles.maxReps}
              // touched={props.touched.maxReps}
              // errors={props.errors.maxReps}
              label="Max reps"
            />
          </View>

          <ValidationTextInput
            onChangeText={handleChange("sets")}
            onBlur={handleBlur("sets")}
            value={props.values.sets}
            style={styles.textInput}
            // touched={props.touched.sets}
            // errors={props.errors.sets}
            label="Sets"
          />

          {(props.errors.exerciseName ||
            props.errors.minReps ||
            props.errors.maxReps ||
            props.errors.sets) && (
            <Text
              style={{
                textAlign: "center",
                marginTop: "4%",
                color: props.theme.colors.error,
              }}
            >
              Fill in the blanks
            </Text>
          )}
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={props.handleSubmit}>Done</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  textInput: { marginTop: "4%" },
  minReps: { marginTop: "4%", width: "47.5%" },
  maxReps: { marginTop: "4%", width: "47.5%", marginLeft: "5%" },
  btn: { marginTop: "10%", alignSelf: "flex-end" },
});

export default withTheme(formikEnhancer(AddExerciseDialog));
