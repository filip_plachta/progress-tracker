import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import {
  withTheme,
  DataTable,
  IconButton,
  FAB,
  Portal,
  Provider,
} from "react-native-paper";
import dayjs from "dayjs";
import { dateFormat } from "../../dateSettings";

function RoutineComponent(props) {
  const { colors } = props.theme;
  const [state, setState] = React.useState({ open: false });

  const onStateChange = ({ open }) => setState({ open });

  const { open } = state;

  return (
    <>
      <View style={[styles.container, { backgroundColor: colors.surface }]}>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title style={{ flex: 1 }}>Actions</DataTable.Title>
            <DataTable.Title>Name</DataTable.Title>
            <DataTable.Title>Created on</DataTable.Title>
          </DataTable.Header>
          {props.routines.map((routine) => {
            return (
              <DataTable.Row key={routine.id}>
                <DataTable.Cell style={{ flex: 1 }}>
                  <IconButton
                    onPress={() => props.onAdd(routine.id)}
                    icon="format-list-bulleted"
                  />
                  {props.mode === "my" && (
                    <IconButton
                      onPress={() => props.deleteRoutine(routine.id)}
                      icon="close"
                    />
                  )}
                  {props.mode === "user" && (
                    <IconButton
                      onPress={() => props.onAddUserPlan(routine.id)}
                      icon="plus"
                    />
                  )}
                </DataTable.Cell>
                <DataTable.Cell>{routine?.name}</DataTable.Cell>
                <DataTable.Cell>
                  {dayjs(routine?.created).format(dateFormat)}
                </DataTable.Cell>
              </DataTable.Row>
            );
          })}
        </DataTable>

        {props.mode === "my" && (
          <Provider>
            <Portal>
              <FAB.Group
                open={open}
                icon={"plus"}
                actions={[
                  {
                    icon: "book-plus",
                    onPress: () => props.onAdd(),
                    label: "Add new routine",
                  },
                  {
                    icon: "history",
                    onPress: () => props.onAddExisting(),
                    label: "Add existing routine",
                  },
                ]}
                onStateChange={onStateChange}
              />
            </Portal>
          </Provider>
        )}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default withTheme(RoutineComponent);
