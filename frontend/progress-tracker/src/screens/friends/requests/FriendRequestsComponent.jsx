import React from "react";
import {
  DataTable,
  IconButton,
  Subheading,
  withTheme,
} from "react-native-paper";
import { View, StyleSheet, ActivityIndicator } from "react-native";

function FriendRequestsComponent(props) {
  const { colors } = props.theme;

  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      {!props.loading &&
        (props.requests?.length > 0 ? (
          <DataTable>
            <DataTable.Header>
              <DataTable.Title style={{ flex: 0.5, textAlign: "center" }}>
                Actions
              </DataTable.Title>
              <DataTable.Title>User</DataTable.Title>
            </DataTable.Header>
            {props.requests.map((request) => {
              return (
                <DataTable.Row key={request?.id}>
                  <DataTable.Cell style={{ flex: 0.5 }}>
                    <IconButton
                      icon="check"
                      onPress={() => {
                        props.submit(request?.login, true);
                      }}
                    />
                    <IconButton
                      icon="close"
                      onPress={() => {
                        props.submit(request?.login, false);
                      }}
                    />
                  </DataTable.Cell>
                  <DataTable.Cell>{request?.login}</DataTable.Cell>
                </DataTable.Row>
              );
            })}
          </DataTable>
        ) : (
          <Subheading style={styles.subheading}>No friend requests.</Subheading>
        ))}

      <ActivityIndicator
        size={50}
        animating={props.loading}
        color={colors.primary}
        style={{ flex: 1, flexDirection: "column" }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  subheading: {
    textAlign: "center",
    marginTop: "5%",
  },
});

export default withTheme(FriendRequestsComponent);
