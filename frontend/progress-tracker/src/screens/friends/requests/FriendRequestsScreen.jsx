import React from "react";
import { NavigationEvents } from "react-navigation";
import {
  getFriendsRequests,
  acceptFriend,
  declineFriend,
} from "../../../actions/friends";
import FriendRequestsComponent from "./FriendRequestsComponent";
import { withSnackbar } from "./../../../components/SnackbarContext";

function FriendRequestsScreen(props) {
  const [requests, setRequests] = React.useState(null);
  const [loading, setLoading] = React.useState(true);

  const fetchData = () => {
    getFriendsRequests().then((res) => {
      setRequests(res?.data);
      setLoading(false);
    });
  };

  const acceptOrDecline = (username, accept) => {
    const submit = accept ? acceptFriend : declineFriend;
    submit({ username })
      .then((res) => {
        props.navigation.pop();
        props.showMessage(res?.data);
      })
      .catch((error) => {
        props.navigation.pop();
        props.showMessage(error?.response?.data?.message);
      });
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      <FriendRequestsComponent
        requests={requests}
        loading={loading}
        submit={acceptOrDecline}
      />
    </>
  );
}

export default withSnackbar(FriendRequestsScreen);
