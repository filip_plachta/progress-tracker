import React from "react";
import { NavigationEvents } from "react-navigation";
import {
  deleteFriendship,
  getMyFriends,
  addFriend,
} from "../../actions/friends";
import { withSnackbar } from "./../../components/SnackbarContext";
import FriendsComponent from "./FriendsComponent";

function FriendsScreen(props) {
  const [friends, setFriends] = React.useState([]);
  const [deleteDialog, setDeleteDialog] = React.useState(false);
  const [toDeleteId, setToDeleteId] = React.useState(null);
  const [addDialog, setAddDialog] = React.useState(false);

  const fetchData = async () => {
    try {
      let response = await getMyFriends();
      setFriends(response?.data);
    } catch (error) {
      props.showMessage(error?.response?.data?.message);
    }
  };

  const openDeleteDialog = (id) => {
    setDeleteDialog(true);
    setToDeleteId(id);
  };

  const closeDeleteDialog = () => {
    setDeleteDialog(false);
    setToDeleteId(null);
  };

  const openAddDialog = () => {
    setAddDialog(true);
  };

  const closeAddDialog = () => {
    setAddDialog(false);
  };

  const deleteFriend = async () => {
    try {
      let response = await deleteFriendship(toDeleteId);
      closeDeleteDialog();
      fetchData();
      props.showMessage(response?.data);
    } catch (error) {
      closeDeleteDialog();
      props.showMessage(error?.response?.data?.message);
    }
  };

  const sendFriendRequest = (username) => {
    addFriend({ username })
      .then((res) => {
        props.showMessage(res.data);
      })
      .catch((error) => {
        props.showMessage(error?.response?.data?.message);
      });
  };

  const goToRequests = () => {
    props.navigation.navigate("FriendRequests");
  };

  const showDetails = (userId) => {
    props.navigation.navigate("UserProfile", {
      userId,
    });
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      <FriendsComponent
        friends={friends}
        openDeleteDialog={openDeleteDialog}
        closeDeleteDialog={closeDeleteDialog}
        deleteFriend={deleteFriend}
        deleteDialog={deleteDialog}
        closeAddDialog={closeAddDialog}
        openAddDialog={openAddDialog}
        addDialog={addDialog}
        sendFriendRequest={sendFriendRequest}
        goToRequests={goToRequests}
        showDetails={showDetails}
      />
    </>
  );
}

export default withSnackbar(FriendsScreen);
