import PropTypes from "prop-types";
import React from "react";
import { View, StyleSheet } from "react-native";
import {
  DataTable,
  IconButton,
  Subheading,
  withTheme,
  Button,
} from "react-native-paper";
import YesNoDialog from "../../components/ui/YesNoDialog";
import AddFriendDialog from "./AddFriendDialog";

function FriendsComponent(props) {
  const { colors } = props.theme;
  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      {props.friends?.length > 0 ? (
        <>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title style={{ flex: 0.5 }}>Actions</DataTable.Title>
              <DataTable.Title>User</DataTable.Title>
            </DataTable.Header>
            {props.friends.map((friend) => {
              return (
                <DataTable.Row key={friend?.id}>
                  <DataTable.Cell style={{ flex: 0.5 }}>
                    <IconButton
                      icon="delete"
                      onPress={() => props.openDeleteDialog(friend.id)}
                    />
                    <IconButton
                      icon="account-arrow-right"
                      onPress={() => props.showDetails(friend.id)}
                    />
                  </DataTable.Cell>
                  <DataTable.Cell>{friend?.login}</DataTable.Cell>
                </DataTable.Row>
              );
            })}
          </DataTable>

          <YesNoDialog
            visible={props.deleteDialog}
            title="Delete friend"
            content="Are you sure you want to delete this friend?"
            onClose={props.closeDeleteDialog}
            noAction={props.closeDeleteDialog}
            yesAction={props.deleteFriend}
          />
        </>
      ) : (
        <Subheading style={styles.text}> No friends yet.</Subheading>
      )}
      <Button
        style={styles.addBtn}
        onPress={props.openAddDialog}
        mode="contained"
      >
        Add new friend
      </Button>
      <Button
        style={styles.addBtn}
        onPress={props.goToRequests}
        mode="outlined"
      >
        Friend requests
      </Button>
      <AddFriendDialog
        visible={props.addDialog}
        onDismiss={props.closeAddDialog}
        sendFriendRequest={props.sendFriendRequest}
      />
    </View>
  );
}

FriendsComponent.propTypes = {
  closeDeleteDialog: PropTypes.func,
  deleteFriend: PropTypes.func,
  friends: PropTypes.array,
  openDeleteDialog: PropTypes.func,
  theme: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  text: {
    textAlign: "center",
    marginTop: "5%",
  },
  addBtn: {
    marginTop: "5%",
  },
});

export default withTheme(FriendsComponent);
