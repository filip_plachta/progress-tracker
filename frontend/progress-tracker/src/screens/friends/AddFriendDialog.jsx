import React from "react";
import { Dialog, TextInput, Button, Portal, Modal } from "react-native-paper";

function AddFriendDialog(props) {
  const [username, setUsername] = React.useState("");
  return (
    <Portal>
      <Dialog visible={props.visible} onDismiss={props.onDismiss}>
        <Dialog.Title>Send friend request</Dialog.Title>
        <Dialog.Content>
          <TextInput
            mode="contained"
            value={username}
            onChangeText={setUsername}
            label="Friend's username"
          ></TextInput>
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            onPress={() => {
              props.onDismiss();
              props.sendFriendRequest(username);
            }}
          >
            Send
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

export default AddFriendDialog;
