import React from "react";
import LoginComponent from "./LoginComponent";
import { auth, setToken } from "../../actions/auth";
import { AuthContext } from "../../components/AuthContext";
import { withSnackbar } from "./../../components/SnackbarContext";

function LoginScreen({ navigation, showMessage }) {
  const [login, setLogin] = React.useState();
  const [password, setPassword] = React.useState();

  const { signIn } = React.useContext(AuthContext);

  const onSubmit = () => {
    let toSubmit = {
      login,
      password,
    };

    auth(toSubmit)
      .then((res) => {
        console.log(res);
        setToken(res?.data?.jwt);
        signIn();
      })
      .catch((error) => {
        showMessage("Wrong password or login");
      });
  };

  const navigateToRegister = () => {
    navigation.navigate("Register");
  };

  return (
    <LoginComponent
      login={login}
      setLogin={setLogin}
      password={password}
      setPassword={setPassword}
      onSubmit={onSubmit}
      navigateToRegister={navigateToRegister}
    />
  );
}

export default withSnackbar(LoginScreen);
