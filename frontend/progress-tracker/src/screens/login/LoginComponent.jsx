import React from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import {
  withTheme,
  Button,
  Title,
  TextInput,
  Divider,
} from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";

function LoginComponent(props) {
  const { colors } = props.theme;
  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: colors.surface }]}
    >
      <StatusBar style={{ color: "white" }} />
      <View>
        <Title style={[styles.headline, { color: colors.primary }]}>
          Progress Tracker
        </Title>
        <Divider />
        <TextInput
          onChangeText={props.setLogin}
          value={props.login}
          style={styles.textInput}
          mode="outlined"
          label="login"
        />
        <TextInput
          onChangeText={props.setPassword}
          secureTextEntry={true}
          value={props.password}
          style={styles.textInput}
          mode="outlined"
          label="password"
        />
        <Button
          onPress={props.onSubmit}
          style={styles.textInput}
          mode="contained"
          // style={styles.btn}
        >
          Sign in
        </Button>
        <Button
          style={styles.btnText}
          onPress={props.navigateToRegister}
          mode="text"
        >
          register
        </Button>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  card: {
    flex: 1,
  },
  headline: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 32,
    marginTop: "50%",
    padding: 4,
  },
  textInput: {
    marginTop: "4%",
  },
  btn: {
    marginTop: "7%",
  },
  btnText: {
    marginTop: "1%",
  },
});

export default withTheme(LoginComponent);
