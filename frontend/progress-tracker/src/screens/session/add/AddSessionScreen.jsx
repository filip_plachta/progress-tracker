import React from "react";
import { NavigationEvents } from "react-navigation";
import { withSnackbar } from "../../../components/SnackbarContext";
import SplashScreen from "../../../components/ui/SplashScreen";
import { initSession, addSession } from "../../../actions/session";
import AddSessionComponent from "./AddSessionComponent";

function AddSessionScreen(props) {
  const [routineId, setRoutineId] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [exercises, setExercises] = React.useState([]);
  const [visible, setVisible] = React.useState(false);
  const [weight, setWeight] = React.useState();
  const [reps, setReps] = React.useState();
  const [notes, setNotes] = React.useState();
  const [exerciseId, setExerciseId] = React.useState();
  const [goalSet, setGoalSet] = React.useState();

  const showDialog = (exercise, goal) => {
    setWeight(goal.performedWeight?.toString());
    setReps(goal.performedReps?.toString());
    setExerciseId(exercise.exercise.id);
    setGoalSet(goal.set);
    setVisible(true);
  };

  const hideDialog = () => {
    setWeight(null);
    setReps(null);
    setNotes(null);
    setVisible(false);
  };

  const onSave = () => {
    let exercisesCopy = exercises;
    let ex = exercisesCopy.filter((el) => el.exercise?.id === exerciseId)[0];

    let goal = ex?.goals?.filter((el) => el.set === goalSet)[0];
    goal.performedReps = parseInt(reps);
    goal.performedWeight = parseFloat(weight);
    goal.notes = notes;
    setExercises(exercisesCopy);
    hideDialog();
  };

  const fetchData = () => {
    const id = props.navigation.getParam("routineId");
    setRoutineId(id);
    initSession(id).then((res) => {
      setExercises(res.data?.exercises);
      setLoading(false);
    });
  };

  const saveSession = () => {
    addSession(routineId, { exercises })
      .then((response) => {
        props.showMessage(response.data);
        props.navigation.pop();
      })
      .catch((error) => {
        props.showMessage(error?.data?.response?.message);
      });
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      {loading ? (
        <SplashScreen />
      ) : (
        <AddSessionComponent
          exercises={exercises}
          showDialog={showDialog}
          hideDialog={hideDialog}
          visible={visible}
          weight={weight}
          reps={reps}
          notes={notes}
          setWeight={setWeight}
          setReps={setReps}
          setNotes={setNotes}
          onSave={onSave}
          saveSession={saveSession}
        />
      )}
    </>
  );
}

export default withSnackbar(AddSessionScreen);
