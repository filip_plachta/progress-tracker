import React from "react";
import { Dialog, Button, Portal, TextInput } from "react-native-paper";

function AddSetDialog(props) {
  return (
    <Portal>
      <Dialog visible={props.visible} onDismiss={props.onDismiss}>
        <Dialog.Title>Add set</Dialog.Title>
        <Dialog.Content>
          <TextInput
            mode="contained"
            value={props.weight}
            onChangeText={props.setWeight}
            label="Weight"
            keyboardType="number-pad"
          ></TextInput>
          <TextInput
            mode="contained"
            value={props.reps}
            onChangeText={props.setReps}
            label="Reps"
            style={{ marginTop: "5%" }}
            keyboardType="number-pad"
          ></TextInput>
          <TextInput
            mode="contained"
            value={props.notes}
            onChangeText={props.setNotes}
            label="Notes"
            style={{ marginTop: "5%" }}
          ></TextInput>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={props.onSave}>Save</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

export default AddSetDialog;
