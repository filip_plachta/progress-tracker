import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { withTheme, List, Card, Button } from "react-native-paper";
import { ScrollView, StyleSheet, View } from "react-native";
import ExerciseTable from "./ExerciseTable";
import AddSetDialog from "./AddSetDialog";

function AddSessionComponent(props) {
  const { colors } = props.theme;
  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: colors.surface }]}
    >
      <ScrollView>
        <List.AccordionGroup>
          {props.exercises?.map((exercise) => (
            <Card key={exercise.exercise.exerciseName} style={styles.card}>
              <List.Accordion
                key={exercise.exercise.exerciseName}
                title={exercise.exercise.exerciseName}
                id={exercise.exercise.id}
              >
                <ExerciseTable
                  exercise={exercise}
                  showDialog={props.showDialog}
                />
              </List.Accordion>
            </Card>
          ))}
        </List.AccordionGroup>
        <AddSetDialog
          visible={props.visible}
          onDismiss={props.hideDialog}
          weight={props.weight}
          reps={props.reps}
          notes={props.notes}
          setWeight={props.setWeight}
          setReps={props.setReps}
          setNotes={props.setNotes}
          onSave={props.onSave}
        />
        <Button mode="contained" onPress={props.saveSession}>
          Save
        </Button>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  card: {
    marginBottom: "5%",
  },
});

export default withTheme(AddSessionComponent);
