import React from "react";
import { DataTable } from "react-native-paper";

function ExerciseTable(props) {
  return (
    <DataTable>
      <DataTable.Header>
        <DataTable.Title style={{ flex: 0.5 }}>Set</DataTable.Title>
        <DataTable.Title>Goal</DataTable.Title>
        <DataTable.Title>Performed</DataTable.Title>
        <DataTable.Title>Notes</DataTable.Title>
      </DataTable.Header>
      {props.exercise?.goals?.map((goal, index) => (
        <DataTable.Row
          onPress={() => props.showDialog(props.exercise, goal)}
          key={index}
        >
          <DataTable.Cell style={{ flex: 0.5 }}>{goal.set}</DataTable.Cell>
          <DataTable.Cell>
            {goal.weight && goal.reps ? `${goal.weight} x ${goal.reps}` : "-"}
          </DataTable.Cell>
          <DataTable.Cell>
            {goal.performedWeight && goal.performedReps
              ? `${goal.performedWeight} x ${goal.performedReps}`
              : "-"}
          </DataTable.Cell>
          <DataTable.Cell>{goal.notes}</DataTable.Cell>
        </DataTable.Row>
      ))}
    </DataTable>
  );
}

export default ExerciseTable;
