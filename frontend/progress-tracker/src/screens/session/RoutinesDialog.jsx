import React from "react";
import { Dialog, Button, Portal, List } from "react-native-paper";

function RoutinesDialog(props) {
  return (
    <Portal>
      <Dialog visible={props.visible} onDismiss={props.onDismiss}>
        <Dialog.Title>Select workout routine</Dialog.Title>
        <Dialog.Content>
          {props.routines?.map((routine) => (
            <List.Item
              key={routine?.id}
              title={routine?.name}
              onPress={() => props.onAdd(routine?.id)}
              left={(props) => <List.Icon {...props} icon="arrow-right" />}
            />
          ))}
        </Dialog.Content>
      </Dialog>
    </Portal>
  );
}

export default RoutinesDialog;
