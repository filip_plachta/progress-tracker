import React from "react";
import { DataTable, Title } from "react-native-paper";

function ExerciseDetailsTable(props) {
  // console.log(props.data);
  return (
    <>
      <Title style={{ marginTop: "2%" }}>{props.exerciseName}</Title>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title style={{ flex: 0.5 }}>Set</DataTable.Title>
          <DataTable.Title>Volume</DataTable.Title>
          <DataTable.Title>Notes</DataTable.Title>
        </DataTable.Header>
        {props.data?.map((el) => {
          return (
            <DataTable.Row key={el?.set}>
              <DataTable.Cell style={{ flex: 0.5 }}>{el?.set}</DataTable.Cell>
              <DataTable.Cell>{`${el?.weight} x ${el?.reps}`}</DataTable.Cell>
              <DataTable.Cell>{el?.notes ?? "-"}</DataTable.Cell>
            </DataTable.Row>
          );
        })}
      </DataTable>
    </>
  );
}

export default ExerciseDetailsTable;
