import React from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import { withTheme, DataTable } from "react-native-paper";
import dayjs from "dayjs";
import { dateFormat } from "../../../dateSettings";
import ExerciseDetailsTable from "./ExerciseDetailsTable";

function SessionDetailsComponent(props) {
  const { colors } = props.theme;
  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        style={[styles.container, { backgroundColor: colors.surface }]}
      >
        {Object.keys(props.exercises).map((exerciseName) => {
          return (
            <ExerciseDetailsTable
              key={exerciseName}
              data={props.exercises[exerciseName]}
              exerciseName={exerciseName}
            />
          );
        })}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  // btn: {
  //   marginTop: 10,
  // },
  // fab: {
  //   position: "absolute",
  //   margin: 16,
  //   right: 0,
  //   bottom: 0,
  // },
});

export default withTheme(SessionDetailsComponent);
