import React from "react";
import { getSession } from "../../../actions/session";
import { NavigationEvents } from "react-navigation";
import SplashScreen from "../../../components/ui/SplashScreen";
import SessionDetailsComponent from "./SessionDetailsComponent";

function SessionDetailsScreen(props) {
  const [sessionInfo, setSessionInfo] = React.useState({});
  const [exercises, setExercises] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [sessionId, setSessionId] = React.useState(true);

  const fetchData = () => {
    const id = props.navigation.getParam("sessionId");
    if (id) {
      setSessionId(id);
      getSession(id).then((res) => {
        setSessionInfo({
          name: res?.data?.routine,
          date: res?.data?.date,
          id: res?.data?.id,
        });
        const ex = groupBy(res?.data?.exercises, "exerciseName");
        setExercises(ex);
        setLoading(false);
      });
    }
  };

  const groupBy = (xs, prop) => {
    var grouped = {};
    for (var i = 0; i < xs.length; i++) {
      var p = xs[i][prop];
      if (!grouped[p]) {
        grouped[p] = [];
      }
      grouped[p].push(xs[i]);
    }
    return grouped;
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      {loading ? (
        <SplashScreen />
      ) : (
        <SessionDetailsComponent
          sessionInfo={sessionInfo}
          exercises={exercises}
        />
      )}
    </>
  );
}

export default SessionDetailsScreen;
