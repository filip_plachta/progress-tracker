import React, { useEffect } from "react";
import SessionComponent from "./SessionComponent";
import { getMySessions, getUserSessions } from "../../actions/session";
import { getMyRoutines } from "../../actions/routine";
import { NavigationEvents, SafeAreaView } from "react-navigation";
import SplashScreen from "../../components/ui/SplashScreen";

export default function SessionScreen({ navigation }) {
  const [sessions, setSessions] = React.useState([]);
  const [routines, setRoutines] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [visible, setVisible] = React.useState(false);
  const [id, setId] = React.useState(null);

  fetchData = () => {
    const userId = navigation.getParam("userId");
    if (userId) {
      setId(userId);
      getUserSessions(userId).then((res) => {
        setSessions(res.data);
        setLoading(false);
      });
    } else {
      getMyRoutines().then((res) => {
        setRoutines(res.data);
      });
      getMySessions().then((res) => {
        setSessions(res.data);
        setLoading(false);
      });
    }
  };

  const showDialog = () => {
    setVisible(true);
  };

  const hideDialog = () => {
    setVisible(false);
  };

  const onAdd = (routineId) => {
    hideDialog();
    navigation.navigate("AddSession", {
      routineId,
    });
  };

  const goToDetails = (sessionId) => {
    if (id) {
      navigation.navigate("UserSessionDetails", {
        sessionId,
      });
    } else {
      navigation.navigate("SessionDetails", {
        sessionId,
      });
    }
  };

  return (
    <>
      <NavigationEvents onDidFocus={fetchData} />
      {loading ? (
        <SplashScreen />
      ) : (
        <SessionComponent
          sessions={sessions}
          onAdd={onAdd}
          showDialog={showDialog}
          hideDialog={hideDialog}
          visible={visible}
          routines={routines}
          goToDetails={goToDetails}
          userId={id}
        />
      )}
    </>
  );
}
