import React from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import {
  withTheme,
  DataTable,
  IconButton,
  Button,
  FAB,
  Portal,
  Provider,
} from "react-native-paper";
import dayjs from "dayjs";
import { dateFormat } from "../../dateSettings";
import RoutinesDialog from "./RoutinesDialog";

function SessionComponent(props) {
  const { colors } = props.theme;
  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        style={[styles.container, { backgroundColor: colors.surface }]}
      >
        <DataTable>
          <DataTable.Header>
            <DataTable.Title style={{ flex: 0.5 }}>Actions</DataTable.Title>
            <DataTable.Title>Routine</DataTable.Title>
            <DataTable.Title>Date</DataTable.Title>
          </DataTable.Header>
          {props.sessions?.map((session) => {
            return (
              <DataTable.Row key={session.id}>
                <DataTable.Cell style={{ flex: 0.5 }}>
                  <IconButton
                    icon="arrow-right"
                    onPress={() => {
                      props.goToDetails(session?.id);
                    }}
                  />
                </DataTable.Cell>
                <DataTable.Cell>{session?.routine}</DataTable.Cell>
                <DataTable.Cell>
                  {dayjs(session?.date).format(dateFormat)}
                </DataTable.Cell>
              </DataTable.Row>
            );
          })}
        </DataTable>
        <RoutinesDialog
          routines={props.routines}
          visible={props.visible}
          onDismiss={props.hideDialog}
          onAdd={props.onAdd}
        />
      </ScrollView>
      {!props.userId && (
        <FAB icon="plus" style={styles.fab} onPress={props.showDialog} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 0,
  },
  btn: {
    marginTop: 10,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default withTheme(SessionComponent);
