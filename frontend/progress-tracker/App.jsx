import React, { useEffect } from "react";
import LoginNavigator from "./src/routes/loginStack";
import AppNavigator from "./src/routes/appStack";
import { DarkTheme, Provider as PaperProvider } from "react-native-paper";
import { AuthContext } from "./src/components/AuthContext";
import SnackbarProvider from "./src/components/SnackbarContext";
import { SafeAreaProvider } from "react-native-safe-area-context";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { axiosDefault } from "./src/actions/axios";
import { getUserData } from "./src/actions/user";
import { ActivityIndicator } from "react-native";
import SplashScreen from "./src/components/ui/SplashScreen";

const theme = {
  ...DarkTheme,

  roundness: 7,
  colors: {
    ...DarkTheme.colors,
    primary: "#ff9800",
    onSurface: "#FFFFFF",
  },
  dark: true,
  mode: "adaptive",
};

export default function App() {
  const [isLogged, setIsLogged] = React.useState(false);
  const [user, setUser] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const authContext = React.useMemo(
    () => ({
      signIn: () => {
        fetchUserData();
      },
      signOut: async () => {
        await AsyncStorage.removeItem("Token");
        delete axiosDefault.defaults.headers.common["Authorization"];
        setIsLogged(false);
      },
      userData: () => {
        return user;
      },
      fetchUserData: () => {
        fetchUserData();
      },
    }),
    [user]
  );

  const fetchUserData = () => {
    getUserData()
      .then((res) => {
        setUser(res.data);
      })
      .catch(() => {
        setIsLogged(false);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (user) {
      setIsLogged(true);
      setLoading(false);
    }
  }, [user]);

  const checkToken = async () => {
    try {
      // const token = `Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNjA2OTUyMzI0LCJpYXQiOjE2MDY5MTYzMjR9.-4TFgnmye2SMF5w3jzQRPk3EUPKm4ozZDTvw6rMRZZ4`; //await AsyncStorage.getItem("Token");
      const token = await AsyncStorage.getItem("Token");
      axiosDefault.defaults.headers.common["Authorization"] = token;
      fetchUserData();
    } catch (e) {
      //no token
      console.log("no token");
      setIsLogged(false);
    }
  };

  useEffect(() => {
    checkToken();
  }, []);

  return (
    <AuthContext.Provider value={authContext}>
      <PaperProvider theme={theme}>
        <SnackbarProvider>
          <SafeAreaProvider>
            {loading ? (
              <SplashScreen />
            ) : !isLogged ? (
              <LoginNavigator theme="dark"></LoginNavigator>
            ) : (
              <AppNavigator theme="dark"></AppNavigator>
            )}
          </SafeAreaProvider>
        </SnackbarProvider>
      </PaperProvider>
    </AuthContext.Provider>
  );
}
